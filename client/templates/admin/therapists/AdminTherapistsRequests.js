import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import { denyUpdateFields } from "../../../../collections/Therapists";

Template.AdminTherapistsRequests.onCreated(function() {
  this.autorun(() => {
    this.subscribe("allLocations");
  });
});

Template.AdminTherapistsRequests.onRendered(function() {
  this.autorun(() => {
    this.subscribe("therapistsRequests", () => {
      Tracker.afterFlush(() => {
        if (Therapists.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[1, 0]],
            headers: {
              0: { sorter: false },
              3: { sorter: false },
              4: { sorter: "customDate" },
              5: { sorter: false },
              6: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminTherapistsRequests.helpers({
  editItem: () => Therapists.findOne(Session.get("editId")),
  editOmitFields: () => denyUpdateFields
});
