import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import { denyUpdateFields } from "../../../../collections/Therapists";

Template.AdminCurrentTherapists.onCreated(function() {
  this.autorun(() => {
    this.subscribe("allLocations");
  });
});

Template.AdminCurrentTherapists.onRendered(function() {
  this.autorun(() => {
    this.subscribe("currentTherapists", () => {
      Tracker.afterFlush(() => {
        if (Therapists.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[1, 0]],
            headers: {
              0: { sorter: false },
              3: { sorter: false },
              4: { sorter: false },
              5: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminCurrentTherapists.helpers({
  editItem: () => Therapists.findOne(Session.get("editId")),
  editOmitFields: () => denyUpdateFields
});
