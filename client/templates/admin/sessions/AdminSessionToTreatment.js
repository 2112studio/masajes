import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";

Template.AdminSessionToTreatment.events({
  "click #session-to-treatment-button"() {
    swal(
      {
        title: "¿Estás seguro?",
        text: "No se pude deshacer esta acción",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E64759",
        confirmButtonText: "Sí, ¡Convertir!"
      },
      () =>
        Meteor.call("treatmentFromSession", this.sessionId, err => {
          if (err) {
            console.log(err);
            Bert.alert(err.reason, "danger", "growl-top-right");
          } else {
            Bert.alert(
              "La sesión se ha convertido en circuito",
              "success",
              "growl-top-right"
            );
          }
        })
    );
  }
});
