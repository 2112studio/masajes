import { Template } from "meteor/templating";
import products from "../../../../common/products";
import { needsPayment } from "../../../helpers";

Template.AdminPendingSessionsRow.helpers({
  isSessionPayed() {
    // All non-treatment sessions are already paid
    if (!this.isTreatment) return true;
    // Find treatment and session number
    const treatment = Clients.findOne({ _id: this.clientId }).treatments.find(
      ({ sessions }) => sessions.find(i => this._id)
    );
    const { type, paidInFull } = treatment;
    // Stop for paid treatments
    if (paidInFull) return true;
    const product = products.find(({ slug }) => slug === type);
    return !needsPayment(this._id, product, treatment);
  }
});
