import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminPastSessions.onCreated(function() {
  this.autorun(() => {
    this.subscribe("pastSessionsClients");
    this.subscribe("pastSessionsTherapists");
    this.subscribe("allLocations");
  });
});

Template.AdminPastSessions.onRendered(function() {
  this.autorun(() => {
    this.subscribe("pastSessions", () => {
      Tracker.afterFlush(() => {
        if (Sessions.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[2, 0]],
            headers: {
              0: { sorter: false },
              2: { sorter: "customDate" },
              6: { sorter: false },
              7: { sorter: false },
              9: { sorter: false },
              10: { sorter: false }
            }
          });
        }
      });
    });
  });
});
