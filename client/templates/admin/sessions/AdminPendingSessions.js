import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminPendingSessions.onCreated(function() {
  this.autorun(() => {
    this.subscribe("pendingSessionsClients");
    this.subscribe("pendingSessionsTherapists");
    this.subscribe("allLocations");
  });
});

Template.AdminPendingSessions.onRendered(function() {
  this.autorun(() => {
    this.subscribe("pendingSessions", () => {
      Tracker.afterFlush(() => {
        if (Sessions.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[2, 0]],
            headers: {
              0: { sorter: false },
              2: { sorter: "customDatetime" },
              3: { sorter: false },
              6: { sorter: false },
              10: { sorter: false },
              11: { sorter: false },
              12: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminPendingSessions.helpers({
  editItem: () => Sessions.findOne(Session.get("editId"))
});
