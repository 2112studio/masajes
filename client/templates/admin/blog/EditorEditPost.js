import { Template } from "meteor/templating";

Template.EditorEditPost.onCreated(function() {
  this.autorun(() => {
    this.subscribe("post", FlowRouter.getParam("id"));
  });
});

Template.EditorEditPost.events({
  "click #update-blog-button"() {
    const body = Session.get("blogText");
    const title = Session.get("blogTitle");
    const description = Session.get("blogDescription");
    const thumbnail = Session.get("blogThumbnail");
    if (!body || !title || !thumbnail || !description) {
      Bert.alert("Todos los campos son obligatorios", "danger");
      return;
    }
    Meteor.call(
      "updatePost",
      FlowRouter.getParam("id"),
      { body, title, thumbnail, description },
      err => {
        if (err) {
          Bert.alert("Hubo un error, intenta de nuevo", "danger");
        } else {
          FlowRouter.go("editor-posts");
        }
      }
    );
  }
});
