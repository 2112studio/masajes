import { Template } from "meteor/templating";

Template.EditorNewPost.events({
  "click #create-blog-button": () => {
    const body = Session.get("blogText");
    const title = Session.get("blogTitle");
    const description = Session.get("blogDescription");
    const thumbnail = Session.get("blogThumbnail");
    if (!body || !title || !thumbnail || !description) {
      Bert.alert("Todos los campos son obligatorios", "danger");
      return;
    }
    Meteor.call("createPost", { body, title, thumbnail, description }, err => {
      if (err) {
        Bert.alert("Hubo un error, intenta de nuevo", "danger");
      } else {
        FlowRouter.go("editor-posts");
      }
    });
  }
});
