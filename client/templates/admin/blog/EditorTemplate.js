import { Template } from "meteor/templating";

Template.EditorTemplate.onRendered(function() {
  const editor = new Quill("#editor", {
    modules: { toolbar: "#toolbar-container" },
    theme: "snow"
  });
  const { title, text, thumbnail, description } =
    Template.instance().data || {};
  if (title) {
    $("#blog-title").val(title);
    Session.set("blogTitle", title);
  } else {
    Session.set("blogTitle", "");
  }
  if (description) {
    $("#blog-description").val(description);
    Session.set("blogDescription", description);
  } else {
    Session.set("blogDescription", "");
  }
  if (text) {
    $(editor.root).html(text);
    Session.set("blogText", text);
  } else {
    Session.set("blogText", "");
  }
  if (thumbnail) {
    Session.set("blogThumbnail", thumbnail);
  } else {
    Session.set("blogThumbnail", "");
  }
  editor.on("text-change", () => {
    const text = $(editor.root).html();
    Session.set("blogText", text);
  });
});

Template.EditorTemplate.events({
  "keyup #blog-title": () => {
    Session.set("blogTitle", $("#blog-title").val());
  },
  "keyup #blog-description": () => {
    Session.set("blogDescription", $("#blog-description").val());
  },
  "change #blog-thumbnail-image": ({ target: { files = [null] } = {} }) => {
    const file = files[0];
    if (
      !file ||
      !["png", "jpg", "jpeg"].reduce(
        (prev, format) => prev || file.type.includes(format),
        false
      )
    ) {
      Bert.alert("Solo se aceptan imagenes. Intenta de nuevo", "danger");
    } else {
      const fr = new FileReader();
      fr.readAsDataURL(file);
      fr.onloadend = () => Session.set("blogThumbnail", fr.result);
    }
  }
});
