import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.EditorBlogPosts.onRendered(function() {
  this.autorun(() => {
    this.subscribe("posts", () => {
      Tracker.afterFlush(() => {
        if (Posts.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[2, 0]],
            headers: {
              0: { sorter: false },
              1: { sorter: false },
              3: { sorter: "customDate" },
              5: { sorter: false }
            }
          });
        }
      });
    });
  });
});
