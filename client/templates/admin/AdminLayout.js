import { Template } from "meteor/templating";

Template.AdminLayout.helpers({
  allowedRole: function() {
    if (this.allowedFor) return this.allowedFor();
    return "admin";
  },
  sidebarElements: () => [
    {
      categoryName: "Sesiones",
      links: [
        {
          label: "Sesiones pendientes",
          route: "admin-pending-sessions",
          role: "admin"
        },
        {
          label: "Asignar sesiones",
          route: "admin-assign-sessions",
          role: "admin"
        },
        {
          label: "Historial de sesiones",
          route: "admin-past-sessions",
          role: "admin"
        }
      ]
    },
    {
      categoryName: "General",
      links: [
        {
          label: "Clientes",
          route: "admin-clients",
          role: "admin"
        },
        {
          label: "Terapeutas",
          route: "admin-therapists",
          role: "admin"
        },
        {
          label: "Códigos de Promoción",
          route: "admin-promo-codes",
          role: "admin"
        },
        {
          label: "Solicitudes de terapeutas",
          route: "admin-therapists-requests",
          role: "admin"
        },
        {
          label: "Prospectos",
          route: "admin-therapists-prospects",
          role: "admin"
        },
        {
          label: "Zonas",
          route: "admin-locations",
          role: "admin"
        },
        {
          label: "Administradores",
          route: "admin-admins",
          role: "superadmin"
        }
      ]
    },
    {
      categoryName: "Blog",
      links: [
        {
          label: "Todas las entradas",
          route: "editor-posts",
          role: "editor"
        },
        {
          label: "Nueva entrada",
          route: "editor-new-post",
          role: "editor"
        }
      ]
    }
  ]
});
