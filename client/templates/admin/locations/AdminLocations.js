import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminLocations.onRendered(function() {
  this.autorun(() => {
    this.subscribe("allLocations", () => {
      Tracker.afterFlush(() => {
        if (Locations.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[0, 0]],
            headers: {
              2: { sorter: false },
              3: { sorter: false },
              4: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminLocations.helpers({
  editItem: () => Locations.findOne(Session.get("editId"))
});
