import { Template } from "meteor/templating";
import { therapistsQuestions } from "../../../../collections/Therapists";
import { Meteor } from "meteor/meteor";

Template.AdminAssignSessions.onCreated(function() {
  this.autorun(() => {
    this.subscribe("pendingUnassignedSessions");
    this.subscribe("clientsWithPendingSessions");
    this.subscribe("currentTherapists");
    this.subscribe("allLocations");
  });
});

Template.AdminAssignSessions.events({
  "click #assign-button": function() {
    const sessionId = Session.get("assignedSession");
    const therapistId = Session.get("assignedTherapist");
    if (!sessionId || !therapistId) return;
    Meteor.call("assignSessionTherapist", sessionId, therapistId, err => {
      if (err) {
        Bert.alert(
          "Hubo un error, intenta de nuevo",
          "danger",
          "growl-top-right"
        );
      } else {
        Bert.alert("Se ha asignado esta sesión", "success", "growl-top-right");
      }
    });
  }
});

Template.AdminAssignSessionsSession.events({
  "click #assign-session-button": function() {
    Session.set("assignedSession", this._id);
  }
});

Template.AdminAssignSessionsTherapist.helpers({
  profileQuestion(slug) {
    return this.profile ? this.profile[`question${slug}`] : "";
  }
});

Template.AdminAssignSessionsTherapist.events({
  "click #assign-therapist-button": function() {
    Session.set("assignedTherapist", this._id);
  }
});
