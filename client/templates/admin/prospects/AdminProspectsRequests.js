import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import { denyUpdateFields } from "../../../../collections/Prospects";

Template.AdminProspectsRequests.onCreated(function() {
  this.autorun(() => {
    this.subscribe("allLocations");
  });
});

Template.AdminProspectsRequests.onRendered(function() {
  this.autorun(() => {
    this.subscribe("prospects", () => {
      Tracker.afterFlush(() => {
        if (Prospects.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[1, 0]],
            headers: {
              0: { sorter: false },
              3: { sorter: "customDate" },
              4: { sorter: false },
              5: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminProspectsRequests.helpers({
  editItem: () => Prospects.findOne(Session.get("editId")),
  editOmitFields: () => denyUpdateFields
});
