import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminClients.onCreated(function() {
  this.autorun(() => {
    this.subscribe("locations");
  });
});

Template.AdminClients.onRendered(function() {
  this.autorun(() => {
    this.subscribe("clients", () => {
      Tracker.afterFlush(() => {
        if (Clients.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[1, 0]],
            headers: {
              0: { sorter: false },
              3: { sorter: false },
              4: { sorter: false },
              5: { sorter: false },
              6: { sorter: false },
              7: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminClients.helpers({
  editItem: () => Clients.findOne(Session.get("editId"))
});
