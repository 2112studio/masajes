import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminPromoCodes.onCreated(function() {
  this.autorun(() => {
    this.subscribe("locations");
  });
});

Template.AdminPromoCodes.onRendered(function() {
  this.autorun(() => {
    this.subscribe("regularPromoCodes", Session.get("currentCity"), () => {
      Tracker.afterFlush(() => {
        if (PromoCodes.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[0, 0]],
            headers: {
              3: { sorter: false },
              6: { sorter: "customDate" },
              7: { sorter: false },
              8: { sorter: false }
            }
          });
        }
      });
    });
  });
});

Template.AdminPromoCodes.helpers({
  editItem: () => PromoCodes.findOne(Session.get("editId"))
});
