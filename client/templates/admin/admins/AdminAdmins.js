import { Meteor } from "meteor/meteor";
import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";

Template.AdminAdmins.onCreated(function() {
  this.autorun(() => {
    this.subscribe("adminCode");
  });
});

Template.AdminAdmins.onRendered(function() {
  this.autorun(() => {
    this.subscribe("admins", () => {
      Tracker.afterFlush(() => {
        if (Meteor.users.find().count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[0, 0]],
            headers: {
              1: { sorter: false },
              2: { sorter: false },
              3: { sorter: false },
            }
          });
        }
      });
    });
  });
});

Template.AdminAdmins.helpers({
  adminLink: () => FlowRouter.url("request-access"),
  adminCode: () => Admins.findOne().code
});

Template.AdminAdmins.events({
  "click #refresh-button": () => Meteor.call("refreshAdminCode")
});
