import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";

Template.AdminAdminsRow.helpers({
  isSuperadmin: function() {
    return Roles.userIsInRole(this._id, "superadmin");
  },
  isAdmin: function() {
    return Roles.userIsInRole(this._id, "admin");
  },
  isEditor: function() {
    return Roles.userIsInRole(this._id, "editor");
  }
});
