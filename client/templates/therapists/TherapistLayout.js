import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";
import { Tracker } from "meteor/tracker";

Template.TherapistLayout.onCreated(function() {
  Tracker.autorun(() => {
    Meteor.subscribe("currentTherapist");
    Meteor.subscribe("therapistOngoingSessions");
  });
});

Template.TherapistLayout.helpers({
  sidebarElements: () => [
    {
      categoryName: "Mi perfil",
      links: [
        {
          label: "Sesiones pendientes",
          route: "therapist-pending-sessions"
        },
        {
          label: "Historial de sesiones",
          route: "therapist-past-sessions"
        }
      ]
    }
  ]
});
