import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";
import { Tracker } from "meteor/tracker";

Template.TherapistCurrentSession.onCreated(function() {
  Tracker.autorun(() => {
    Meteor.subscribe("ongoingSessionClient");
  });
});
