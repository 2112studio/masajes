import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import { pastStatus } from "../../../../common/businessLogic";

Template.TherapistPastSessions.onCreated(function() {
  this.autorun(() => {
    // In case some info is needed
    // this.subscribe("pastSessionsClients");
    this.subscribe("allLocations");
  });
});

Template.TherapistPastSessions.onRendered(function() {
  this.autorun(() => {
    this.subscribe("therapistPastSessions", () => {
      Tracker.afterFlush(() => {
        if (Sessions.find({ status: { $in: pastStatus } }).count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[0, 0]],
            headers: {
              0: { sorter: "customDate" },
              2: { sorter: false },
              3: { sorter: false }
            }
          });
        }
      });
    });
  });
});
