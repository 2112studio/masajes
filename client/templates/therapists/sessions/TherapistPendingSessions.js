import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import { pendingStatus } from "../../../../common/businessLogic";

Template.TherapistPendingSessions.onCreated(function() {
  this.autorun(() => {
    this.subscribe("therapistPendingSessionsClients");
    this.subscribe("locations");
  });
});

Template.TherapistPendingSessions.onRendered(function() {
  this.autorun(() => {
    this.subscribe("therapistPendingSessions", () => {
      Tracker.afterFlush(() => {
        if (Sessions.find({ status: { $in: pendingStatus } }).count()) {
          $("[data-sort=table]").tablesorter({
            sortList: [[2, 0]],
            headers: {
              0: { sorter: false },
              2: { sorter: "customDatetime" },
              3: { sorter: false },
              4: { sorter: false }
            }
          });
        }
      });
    });
  });
});
