import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";

Template.StartSessionButton.events({
  "click #start-session-button": () => Meteor.call("startSession")
});
