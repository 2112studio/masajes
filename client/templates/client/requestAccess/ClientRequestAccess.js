import { Meteor } from "meteor/meteor";

Template.ClientRequestAccess.events({
  "click #request-access-button": () => {
    Meteor.call("requestAdmin", $("#request-access-input").val(), err => {
      if (err) {
        Bert.alert("Código incorrecto. Intenta de nuevo", "danger");
      }
    });
  }
});
