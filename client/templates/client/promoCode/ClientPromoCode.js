import { Tracker } from "meteor/tracker";
import { Template } from "meteor/templating";
import ClipboardJS from "clipboard";

Template.ClientPromoCode.onCreated(function() {
  Tracker.autorun(() => {
    Meteor.subscribe("currentClient");
    Meteor.subscribe("clientPromoCode");
  });
});

Template.ClientPromoCode.onRendered(function() {
  const clipboard = new ClipboardJS("#promoCodeBox");
  clipboard.on("success", function(e) {
    swal({
      title: "¡Éxito!",
      text:
        "Tu codigo ha sido copiado. Ahora ya puedes compartirlo en Whatsapp, Facebook o donde quieras!",
      type: "success"
    });
  });
});

Template.ClientPromoCode.events({
  "click #promoCodeBox": () => {}
});
