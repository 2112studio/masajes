import { Template } from "meteor/templating";
import { Tracker } from "meteor/tracker";
import Sessions from "../../../../collections/Sessions";
import { needsPayment } from "../../../helpers";

Template.ClientPurchase.onCreated(function() {
  Tracker.autorun(() => {
    Meteor.subscribe("clientCurrentTreatmentSessions");
  });
});

Template.ClientPurchase.helpers({
  hasCurrentTreatment: client =>
    client &&
    client.treatments.reduce(
      (prev, treatment) =>
        prev || (treatment.isCurrent && !treatment.isFinished),
      false
    ),
  treatmentNeedsPayment: (treatment, product) => {
    const { sessions, paidInFull } = treatment;
    const { partialPaymentsSessions } = product;
    const sessionNumber =
      sessions.findIndex(i => {
        const session = Sessions.findOne(i);
        return session && session.status === "confirmed";
      }) + 1;

    // Session 1 is already paid
    if (sessionNumber <= 1) return false;
    // Find the session number on the payments array
    const nextPartialPayment = partialPaymentsSessions.find(
      i => i === sessionNumber
    );
    // If session is not found, doesn't require payment
    if (!nextPartialPayment) return false;
    // Find the index of the payment
    const paymentIndex = partialPaymentsSessions.findIndex(
      i => i === sessionNumber
    );
    // Check if is last payment and already paid
    if (paymentIndex === partialPaymentsSessions.length - 1 && paidInFull) {
      return false;
    }
    const userNeedsToPay = needsPayment(
      sessions[sessionNumber - 1],
      product,
      treatment
    );
    if (userNeedsToPay) {
      return paymentIndex;
    }
  },
  treatmentPendingPayment: (
    { amountPaid },
    { pendingPromos },
    { partialPayments, price, referralDiscounts },
    index
  ) => {
    const withGiftCard =
      Session.get("useGiftCard") === "si"
        ? Clients.findOne().giftCardAmount
        : 0;
    const minZero = amount => {
      if (amount <= 0) {
        Session.set("noPayment", "si");
        return 0;
      } else {
        Session.set("noPayment", "no");
        return amount;
      }
    };
    if (index === partialPayments.length - 1) {
      if (pendingPromos.length)
        return minZero(
          price -
            referralDiscounts[pendingPromos.length - 1] -
            amountPaid -
            withGiftCard
        );
      else return minZero(price - amountPaid - withGiftCard);
    } else {
      return minZero(partialPayments[index] * price - withGiftCard);
    }
  },
  isLastPayment: ({ partialPayments }, index) =>
    index === partialPayments.length - 1,
  accumulatedDiscount: (
    { pendingPromos },
    { referralDiscounts },
    { discountsApplied }
  ) => {
    if (pendingPromos.length) {
      return referralDiscounts[pendingPromos.length - 1] + discountsApplied;
    }
  }
});

Template.ClientPurchase.events({
  "click #pay-treatment-button": () => {
    const sameCard = Session.get("sameCard");
    const noPayment = Session.get("noPayment") === "si";
    const { last4 } = Clients.findOne() || {};
    let error = false;

    if (!noPayment && (sameCard !== "si" || !last4)) {
      cardInputs.map(i => {
        if (!$(`#${i}`).val()) {
          error = true;
        }
      });
    }

    if (error) {
      Bert.alert("Por favor completa todos los campos de la orden", "danger");
      return;
    }

    if (!noPayment && (sameCard !== "si" || !last4)) {
      Session.set(
        "last4",
        $("#card-number")
          .val()
          .split("")
          .reverse()
          .slice(0, 4)
          .reverse()
          .join("")
      );
    }

    // Previene hacer submit más de una vez
    $("#pay-treatment-button").prop("disabled", true);
    if (!noPayment && (sameCard !== "si" || !last4)) {
      Conekta.Token.create(
        $("#credit-card-form"),
        conektaSuccessResponseHandler,
        conektaErrorResponseHandler
      );
    } else {
      conektaSuccessResponseHandler();
    }
    return false;
  }
});

const conektaSuccessResponseHandler = ({ id: token } = {}) => {
  const noPayment = Session.get("noPayment") === "si";
  const useGiftCard = Session.get("useGiftCard") === "si";
  const sameCard = Session.get("sameCard");
  const { last4: clientLast4 } = Clients.findOne() || {};
  const last4 = Session.get("last4");

  Session.set("last4", null);

  Meteor.call(
    "payTreatment",
    {
      token,
      last4,
      sameCard: sameCard === "si" && clientLast4,
      noPayment,
      useGiftCard
    },
    err => {
      if (err) {
        console.log(err);
        Bert.alert(
          "Lo sentimos, hubo un error. Por favor intenta de nuevo",
          "danger"
        );
      } else {
        Bert.alert("¡Felicidades! Tu pago ha sido exitoso", "success");
        FlowRouter.go("client-profile");
      }
    }
  );
};

const conektaErrorResponseHandler = response => {
  Bert.alert(response.message_to_purchaser, "danger");
  $("#pay-treatment-button").prop("disabled", false);
};
