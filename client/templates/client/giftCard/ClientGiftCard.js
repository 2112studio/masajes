import { Template } from "meteor/templating";

Template.ClientGiftCard.events({
  "click #client-add-gift": () => {
    Meteor.call("redeemGiftCard", $("#client-gift-card").val(), err => {
      if (err) {
        Bert.alert("Hubo un error con tu código, intenta de nuevo", "danger");
      } else {
        Bert.alert(
          "¡Felicidades! Tu certificado de regalo ha sido activado",
          "success"
        );
      }
    });
  }
});
