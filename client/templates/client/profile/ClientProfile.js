import { Template } from "meteor/templating";
import {
  pendingStatus,
  surveyQuestions
} from "../../../../common/businessLogic";

Template.ClientProfile.onCreated(function() {
  this.autorun(() => {
    this.subscribe("clientSessions", Meteor.userId());
    this.subscribe("allLocations");
  });
});

const findMoreCommonWord = data => {
  const results = data.reduce(
    (prev, curr) => ({
      ...prev,
      ...(prev[curr] ? { [curr]: prev[curr] + 1 } : { [curr]: 1 })
    }),
    {}
  );
  let result;
  let maxResult = 0;
  for (const [key, value] of Object.entries(results)) {
    if (value > maxResult) {
      result = key;
    }
  }
  return result;
};

const parseWord = word =>
  word
    .toLowerCase()
    .replace(/á/g, "a")
    .replace(/é/g, "e")
    .replace(/í/g, "i")
    .replace(/ó/g, "o")
    .replace(/ú/g, "u");

Template.ClientProfile.helpers({
  questionsBeforeData: () => {
    const sessions = Sessions.find().fetch();
    // Create a hash with the results for each questions, used to count the most popular. It's called "general array" from now on
    const dataAnswers = surveyQuestions.reduce(
      (prev, { slug, resultsLabel }) => ({
        ...prev,
        [slug]: { label: resultsLabel, data: [] }
      }),
      {}
    );
    // Filter sessions with data
    const beforeSessionsWithData = sessions.filter(
      ({ checkInSurvey }) => checkInSurvey
    );
    if (beforeSessionsWithData.length) {
      beforeSessionsWithData.map(({ checkInSurvey }) =>
        surveyQuestions.map(({ slug, options, bank }) => {
          if (options) {
            // When questions has scoped options, just add the current answer to the general array
            dataAnswers[slug].data.push(checkInSurvey[slug]);
          } else {
            // This is for open questions
            checkInSurvey[slug]
              // Create an array of words from phrase
              .split(" ")
              // Filter known words by the word bank of the question
              .filter(i => bank[parseWord(i)])
              // Add each recognized word to the general array
              .map(i => dataAnswers[slug].data.push(parseWord(i)));
          }
        })
      );
      // Iterate each question again to get results
      surveyQuestions.map(({ slug, options, bank }) => {
        // Set the final result of the question
        dataAnswers[slug].result = options
          ? // If options are set, the responses are in human readable format, so no need to parse
            findMoreCommonWord(dataAnswers[slug].data)
          : // If question has a bank, answers are in slug format, so we need to parse the result to a human readable string
            bank[findMoreCommonWord(dataAnswers[slug].data)];
      });
      // Create array to be read by template with label and answer
      const res = [];
      for (const { label, result } of Object.values(dataAnswers)) {
        res.push({ label, result });
      }
      return res;
    }
  },
  questionsAfterData: () => {
    const sessions = Sessions.find().fetch();
    const dataAnswers = surveyQuestions.reduce(
      (prev, { slug, resultsLabel }) => ({
        ...prev,
        [slug]: { label: resultsLabel, data: [] }
      }),
      {}
    );
    const beforeSessionsWithData = sessions.filter(
      ({ checkOutSurvey }) => checkOutSurvey
    );
    if (beforeSessionsWithData.length) {
      beforeSessionsWithData.map(({ checkOutSurvey }) =>
        surveyQuestions.map(({ slug, options, bank }) => {
          if (options) {
            dataAnswers[slug].data.push(checkOutSurvey[slug]);
          } else {
            checkOutSurvey[slug]
              .split(" ")
              .filter(i => bank[parseWord(i)])
              .map(i => dataAnswers[slug].data.push(parseWord(i)));
          }
        })
      );
      surveyQuestions.map(({ slug, options, bank }) => {
        dataAnswers[slug].result = options
          ? findMoreCommonWord(dataAnswers[slug].data)
          : bank[findMoreCommonWord(dataAnswers[slug].data)];
      });
      const res = [];
      for (const { label, result } of Object.values(dataAnswers)) {
        res.push({ label, result });
      }
      return res;
    }
  },
  currLevel: () => {
    const { treatments } = Clients.findOne();
    return (treatments.length > 4 ? 4 : treatments.length) - 1;
  },
  accumulatedDiscount: ({ pendingPromos }, { referralDiscounts }) => {
    if (pendingPromos.length) {
      return referralDiscounts[pendingPromos.length - 1];
    }
  },
  // Return as an array so it works along with #each helper
  nextSession: () =>
    Sessions.find(
      { status: { $in: pendingStatus } },
      { sort: { scheduledDate: 1 }, limit: 1 }
    ),
  hasPendingSessions: () =>
    Sessions.find({ status: { $in: pendingStatus } }).count()
});
