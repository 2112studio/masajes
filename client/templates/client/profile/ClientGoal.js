import { Template } from "meteor/templating";
import { genericAdminError } from "../../../general";

Template.ClientGoal.events({
  "click #set-goal-button": () =>
    Meteor.setTimeout(function() {
      $("#set-goal-modal").modal("show");
    }, 500),
  "click #set-goal-save-button"() {
    const treatmentIndex = Clients.findOne().treatments.findIndex(
      ({ isCurrent }) => isCurrent
    );
    Meteor.call(
      "setTreatmentGoal",
      treatmentIndex,
      $("#set-goal-input").val(),
      err => {
        if (err) {
          genericAdminError();
        } else {
          Bert.alert("¡Felicidades! Tu objetivo ha sido guardad", "success");
          $("#set-goal-modal").modal("hide");
        }
      }
    );
  }
});
