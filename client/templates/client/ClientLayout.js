import { Template } from "meteor/templating";
import { Tracker } from "meteor/tracker";
import { clientQuestions } from "../../../collections/Clients";

Template.ClientLayout.onCreated(function() {
  Tracker.autorun(() => {
    Meteor.subscribe("currentClient");
    Meteor.subscribe("clientOngoingSessions");
    Meteor.subscribe("locations");
  });
});

Template.ClientLayout.helpers({
  missingProfile: () => {
    const client = Clients.findOne();
    if (client && client.profile)
      return clientQuestions.reduce(
        (prev, { required, slug }) =>
          prev || (required && !client.profile[`question${slug}`]),
        false
      );
    return true;
  },
  sidebarElements: () => [
    {
      categoryName: "",
      links: [
        {
          label: "Mi perfil",
          route: "client-profile"
        },
        {
          label: "Reservar",
          route: "client-purchase"
        },
        {
          label: "Mi historial",
          route: "client-history"
        },
        {
          label: "Código de referencia",
          route: "client-promo-code"
        },
        {
          label: "Editar mis datos",
          route: "client-user-profile"
        },
        {
          label: "Certificados de Regalo",
          route: "client-gift"
        }
      ]
    }
  ]
});
