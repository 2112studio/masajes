import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";
import { surveyQuestions } from "../../../../common/businessLogic";

Template.ClientCurrentSession.helpers({
  questions: surveyQuestions
});

const callAnswer = err => {
  if (err) {
    Bert.alert("Hubo un error, intenta de nuevo", "danger");
  } else {
    surveyQuestions.map(({ slug, options }) => {
      if (options) {
        Session.set(`survey-${slug}`, null);
      } else {
        $(`#${slug}-input`).val("");
      }
    });
    $("#start-survey-modal").modal("hide");
  }
};

Template.ClientCurrentSession.events({
  "click #start-session-button": () => {
    Session.set("startSurvey", true);
    Meteor.setTimeout(function() {
      $("#start-survey-modal").modal("show");
    }, 500);
  },
  "click #start-survey-button": () => {
    let error = false;
    const answers = surveyQuestions.reduce((prev, { slug, options }) => {
      let answer;
      if (options) {
        answer = Session.get(`survey-${slug}`);
      } else {
        answer = $(`#${slug}-input`).val();
      }
      if (!answer) error = true;
      return { ...prev, [slug]: answer };
    }, {});
    if (error) {
      Bert.alert("Todas las preguntas son obligatorias", "danger");
    } else {
      if (Session.get("startSurvey")) {
        Meteor.call("addSurvey", answers, true, err => {
          callAnswer(err);
        });
      } else {
        Meteor.call(
          "finishSession",
          { ...answers, practice: $("#practice-input").val() },
          err => {
            callAnswer(err);
          }
        );
      }
    }
  },
  "click #finish-session-button": () => {
    Session.set("startSurvey", false);
    Meteor.setTimeout(function() {
      $("#start-survey-modal").modal("show");
    }, 500);
  }
});
