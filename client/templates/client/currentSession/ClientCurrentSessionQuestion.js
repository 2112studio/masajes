import { Meteor } from "meteor/meteor";
import { Template } from "meteor/templating";

Template.ClientCurrentSessionQuestionOption.helpers({
  selectedItem() {
    const value = Session.get(`survey-${this.slug}`);
    return value === this.label;
  }
});

Template.ClientCurrentSessionQuestion.events({
  "click #option-item"() {
    Session.set(`survey-${this.slug}`, this.label);
  }
});
