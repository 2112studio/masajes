import { Template } from "meteor/templating";
import { Session } from "meteor/session";
import products from "../../../../../common/products";
import { cardInputs } from "../../../shared/purchase/Purchase";

Template.LandingGiftCard.onRendered(() => {
  Session.set("giftCardProduct", null);
  Session.set("successPurchase", null);
});

Template.LandingGiftCard.helpers({
  prices: products.map(({ title, price }) => ({ title, price })),
  isDisabled: () => {
    const amount = Session.get("giftCardAmount");
    return amount ? {} : { disabled: "disabled" };
  }
});

Template.LandingGiftCard.events({
  "click #gift-card-other-button"() {
    Session.set("giftCardAmount", null);
    Session.set("giftCardProduct", null);
    Session.set("giftCardFixed", false);
  },
  "change #gift-card-amount": event =>
    Session.set("giftCardAmount", Number(event.target.value)),
  "click #pay-gift-card-button": () => {
    let error = false;
    const amount = Session.get("giftCardAmount");
    const email = $("#gift-card-email").val();
    const cardHolderEmail = $("#gift-card-cardholder-email").val();
    cardInputs.map(i => {
      if (!$(`#${i}`).val()) {
        error = true;
      }
    });

    if (!amount || !email || !cardHolderEmail || error) {
      Bert.alert("Por favor completa todos los campos", "danger");
      return;
    }

    // Previene hacer submit más de una vez
    $("#pay-gift-card-button").prop("disabled", true);
    Conekta.Token.create(
      $("#credit-card-form"),
      conektaSuccessResponseHandler,
      conektaErrorResponseHandler
    );
    return false;
  }
});

Template.LandingGiftCardItem.events({
  "click #gift-card-button"() {
    Session.set("giftCardAmount", this.price);
    Session.set("giftCardProduct", this.title);
    Session.set("giftCardFixed", true);
  }
});

const conektaSuccessResponseHandler = ({ id: token } = {}) => {
  const email = $("#gift-card-email").val();
  const name = $("#gift-card-name").val();
  const amount = Session.get("giftCardAmount");
  const cardholder = $("#card-name").val();
  const cardHolderEmail = $("#gift-card-cardholder-email").val();

  Meteor.call(
    "purchaseGiftCard",
    { email, name, amount, cardholder, token, cardHolderEmail },
    err => {
      if (err) {
        Bert.alert(err.reason, "danger");
        $("#pay-gift-card-button").prop("disabled", false);
      } else {
        Bert.alert("¡Felicidades! Tu orden ha sido exitosa", "success");
        Session.set("successPurchase", true);
      }
    }
  );
};

const conektaErrorResponseHandler = response => {
  Bert.alert(response.message_to_purchaser, "danger");
  $("#pay-gift-card-button").prop("disabled", false);
};
