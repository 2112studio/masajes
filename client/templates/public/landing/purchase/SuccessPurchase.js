import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.SuccessPurchase.onCreated(function() {
  this.autorun(() => {
    this.subscribe("locations");
  });
});

Template.SuccessPurchase.onRendered(() => {
  if (!Session.get("orderSummary")) FlowRouter.go("home");
});
