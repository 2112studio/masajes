import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.LandingPurchase.onCreated(function() {
  this.autorun(() => {
    this.subscribe("locations");
  });
});

Template.LandingPurchase.onRendered(() => {
  if (!Session.get("hasAddress")) Session.set("hasAddress", "si");
});

Template.LandingPurchase.helpers({
  redirect: () => Meteor.userId() && FlowRouter.go("admin")
});

Template.LandingPurchase.events({
  "click #button-go-gift-card": () => FlowRouter.go("gift-card")
});
