import { Template } from "meteor/templating";

Template.LandingNavbar.events({
  "click .nav-item": () => {
    $(".navbar-toggler").click();
  },
  "click .navbar-brand": () => {
    if ($(".navbar-collapse").hasClass("show")) $(".navbar-toggler").click();
  }
});
