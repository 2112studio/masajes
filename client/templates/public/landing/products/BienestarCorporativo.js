import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";

const fields = [
  { input: "register-name", question: "Nombre" },
  { input: "register-empresa", question: "Empresa" },
  { input: "register-position", question: "Puesto" },
  { input: "register-phone", question: "Teléfono de Contacto", type: "tel" },
  { input: "register-email", question: "Email", type: "email" },
  {
    input: "register-quantity",
    question: "Cantidad de colaboradores",
    type: "number"
  },
  {
    input: "register-reason",
    question:
      "¿Cuál es la razón por la que buscas nuestro programa de Bienestar Corporativo?"
  }
];

Template.BienestarCorporal.helpers({
  heroText: () => `
    Tu negocio es importante y también lo son tus empleados, 
    quienes lo hacen <span class="hero-description-big">grande</span>
  `,
  benefits: () => [
    {
      title: "Incrementa",
      data: [
        "Rendimiento",
        "Productividad",
        "Compromiso de tu talento",
        "Fidelidad",
        "Calidad de vida",
        "Satisfacción",
        "Ambiente de trabajo",
        "Imagen de tu negocio",
        "Mejor toma de decisiones"
      ]
    },
    {
      title: "Reduce",
      data: [
        "Absentismo",
        "Bajas Laborales",
        "Errores en toma de decisiones",
        "Estrés",
        "Rotación",
        "Problemas para trabajar en Equipo"
      ]
    }
  ],
  regiserFields: () => fields
});

Template.BienestarCorporal.events({
  "click #hero-button": () => {
    $("#corporal-registro").modal("show");
  },
  "submit #corporal-registro form": event => {
    event.preventDefault();
    let error = false;
    const values = fields.map(({ question, input }) => {
      const value = $(`#${input}`).val();
      if (!value) {
        error = true;
      }
      return {
        question,
        value
      };
    });
    if (error) {
      Bert.alert("Todos los campos son obligatorios", "danger");
    } else {
      Meteor.call("sendBienestarCorporativo", values);
      $("#corporal-registro").modal("hide");
      Bert.alert(
        "Felicidades, tu cotización está en proceso. Te contactaremos en breve",
        "success"
      );
    }
  }
});
