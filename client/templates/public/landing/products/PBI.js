import { Template } from "meteor/templating";
import products from "../../../../../common/products";

Template.PBI.helpers({
  product: () => products.find(({ slug }) => slug === "pbi")
});

Template.PBI.events({
  "click #hero-button": () => {
    const product = products.find(({ slug }) => slug === "pbi");
    Session.set("selectedProduct", product);
    scrollTop();
    FlowRouter.go("purchase");
  }
});
