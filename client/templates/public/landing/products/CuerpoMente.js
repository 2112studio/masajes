import { Template } from "meteor/templating";
import products from "../../../../../common/products";

Template.CuerpoMente.helpers({
  product: () => products.find(({ slug }) => slug === "cuerpomente")
});

Template.CuerpoMente.events({
  "click #hero-button": () => {
    const product = products.find(({ slug }) => slug === "cuerpomente");
    Session.set("selectedProduct", product);
    FlowRouter.go("purchase");
  }
});
