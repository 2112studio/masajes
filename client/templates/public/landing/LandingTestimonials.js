import { Template } from "meteor/templating";

Template.LandingTestimonials.onCreated(function() {
  this.currSelected = new ReactiveVar(0);
});

Template.LandingTestimonials.helpers({
  currSelected: () => Template.instance().currSelected.get(),
  isSelected: (a, b) => (a === b ? "selected" : ""),
  selectedTestimony() {
    return this.testimonials.find(
      ({ index }) => index === Template.instance().currSelected.get()
    );
  },
  notFirstSelected: () => Template.instance().currSelected.get() !== 0,
  notLastSelected() {
    return (
      Template.instance().currSelected.get() !== this.testimonials.length - 1
    );
  }
});

Template.LandingTestimonials.events({
  "click .testimonial-image"() {
    Template.instance().currSelected.set(this.index);
  },
  "click #testimonial-left": () =>
    Template.instance().currSelected.set(
      Template.instance().currSelected.get() - 1
    ),
  "click #testimonial-right": () =>
    Template.instance().currSelected.set(
      Template.instance().currSelected.get() + 1
    )
});
