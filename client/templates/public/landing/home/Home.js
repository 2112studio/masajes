import { Template } from "meteor/templating";
import { generalTestimonies } from "../../../../../common/businessLogic";

Template.Home.onRendered(() => {
  const { context: { hash = "" } = {} } = FlowRouter.current();
  if (hash) {
    easeScroll(`#${hash}`);
  }
});

Template.Home.helpers({
  testimonials: () => generalTestimonies
});
