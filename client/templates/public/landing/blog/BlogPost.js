import { Template } from "meteor/templating";
import Posts from "../../../../../collections/Posts";

Template.BlogPost.onCreated(function() {
  this.autorun(() => {
    this.subscribe("postBySlug", FlowRouter.getParam("slug"));
    this.subscribe("popularPosts", 3);
    this.subscribe("postsLimit", 3); // Recent posts
  });
  Meteor.call("addVisitPost", FlowRouter.getParam("slug"));
});

Template.BlogPost.helpers({
  opts: () => ({
    facebook: true,
    twitter: true,
    pinterest: true,
    shareData: {
      // Get the url of current route
      url: Meteor.absoluteUrl() + FlowRouter.current().path.slice(1)
    }
  }),
  currPost: () => Posts.findOne({ slug: FlowRouter.getParam("slug") }),
  popularPosts: () =>
    Posts.find(
      {},
      {
        sort: {
          views: -1
        },
        limit: 3
      }
    ),
  recentPosts: () =>
    Posts.find(
      {},
      {
        sort: {
          createdAt: -1
        },
        limit: 3
      }
    )
});
