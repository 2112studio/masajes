import { Template } from "meteor/templating";
import Posts from "../../../../../collections/Posts";

Template.BlogHome.onCreated(function() {
  this.posts = new ReactiveVar(10);
  this.hasMore = new ReactiveVar(true);
  this.autorun(() => {
    this.subscribe("postsLimit", this.posts.get());
  });
});

Template.BlogHome.helpers({
  hasMore: () => Posts.find().count() && Template.instance().hasMore.get()
});

Template.BlogHome.events({
  "click #view-more-button": () => {
    const currLen = Posts.find().count();
    const { hasMore, posts } = Template.instance();
    posts.set(posts.get() + 10);
    setTimeout(() => {
      if (currLen === Posts.find().count()) {
        hasMore.set(false);
      }
    }, 300);
  }
});
