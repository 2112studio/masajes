import { Template } from "meteor/templating";

const field = [
  { input: "newsletter-name", question: "Nombre" },
  { input: "newsletter-email", question: "Email", type: "email" }
];

Template.LandingFooter.onRendered(() => {
  $("img.svg").each(function() {
    const $img = $(this);
    const imgID = $img.attr("id");
    const imgClass = $img.attr("class");
    const imgURL = $img.attr("src");
    const dataStar = $img.attr("data-star");

    $.get(
      imgURL,
      function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find("svg");

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== "undefined") {
          $svg = $svg.attr("id", imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr("xmlns:a");

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (
          !$svg.attr("viewBox") &&
          $svg.attr("height") &&
          $svg.attr("width")
        ) {
          $svg.attr(
            "viewBox",
            "0 0 " + $svg.attr("height") + " " + $svg.attr("width")
          );
        }

        $svg
          .attr({
            width: "16%",
            height: "100%",
            "data-star": dataStar
          })
          .addClass("star");

        // Replace image with new SVG
        $img.replaceWith($svg);
      },
      "xml"
    );
  });
});

Template.LandingFooter.helpers({
  links: () => [
    { label: "Blog", link: "blog" },
    { label: "Carreras", link: "careers" },
    { label: "Aviso de Privacidad", link: "privacy" }
  ],
  socialNetworks: () => [
    { icon: "facebook", link: "#" },
    { icon: "instagram", link: "#" },
    { icon: "youtube", link: "#" },
    { icon: "whatsapp", link: "#" },
    { icon: "pinterest", link: "#" }
  ],
  newsletterFields: () => field
});

Template.LandingFooter.events({
  "submit #newsletter-modal form": event => {
    event.preventDefault();
    let error = false;
    const values = field.map(({ question, input }) => {
      const value = $(`#${input}`).val();
      if (!value) {
        error = true;
      }
      return {
        question,
        value
      };
    });
    if (error) {
      Bert.alert("Todos los campos son obligatorios", "danger");
    } else {
      // TODO: Call email
      console.log({ values });
      $("#newsletter-modal").modal("hide");
      Bert.alert("Felicidades, tu suscripción ha sido exitosa", "success");
    }
  }
});
