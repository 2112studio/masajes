import { Template } from "meteor/templating";

Template.CareersHeader.helpers({
  data: () => [
    {
      emoji: "💆‍",
      text: "Aprende nuestro masaje Terapéutico Integral CuerpoMente®"
    },
    {
      emoji: "💲",
      text: "Gana lo que te mereces"
    },
    {
      emoji: "🕐",
      text:
        "Trabaja en tiempos flexibles a tu vida y ¡ten más tiempo para hacer otras cosas!"
    },
    {
      emoji: "👍🏻",
      text: "Desarróllate en tus estudios como Terapeuta Corporal"
    },
    {
      emoji: "😌",
      text: "Crece dentro de un equipo de paz y profesionalismo"
    },
    { emoji: "😃", text: "¡Sé feliz y diviértete!" }
  ]
});
