import { Template } from "meteor/templating";

Template.Careers.onCreated(function() {
  this.isTherapist = new ReactiveVar(true);
  this.autorun(() => {
    this.subscribe("allLocations");
  });
});

Template.Careers.helpers({
  isTherapist: () => Template.instance().isTherapist.get()
});

Template.Careers.events({
  "click .toggle-careers-button": () => {
    const { isTherapist } = Template.instance();
    Template.instance().isTherapist.set(!isTherapist.get());
  }
});
