import { Template } from "meteor/templating";

Template.ActiveButton.helpers({
  parsedTextOn: function() {
    if (this.textOn) return this.textOn;
    return "Activo";
  },
  parsedTextOff: function() {
    if (this.textOff) return this.textOff;
    return "Desactivado";
  }
});

Template.ActiveButton.events({
  "click #toggle-active-button": function() {
    if (!this.disabled) {
      Meteor.call(this.method, this.id, err => {
        if (err) {
          Bert.alert(
            "Hubo un error, intenta de nuevo",
            "danger",
            "growl-top-right"
          );
        } else {
          Bert.alert(
            "El elemento ha cambiado de estatus",
            "success",
            "growl-top-right"
          );
        }
      });
    }
  }
});
