import { Template } from "meteor/templating";
import { Session } from "meteor/session";
import { Meteor } from "meteor/meteor";
import times from "lodash.times";
import {
  getPurchasePrice,
  addressFields,
  accountFields
} from "../../../../common/businessLogic";
import { prettyPrice } from "../../../helpers";

const isValid = ({ product, hasAddress, splitPayment, goal }) => {
  let error = false;
  if (!product) error = true;
  else {
    if (product.isTreatment) {
      if (!goal) error = true;
      if (!splitPayment) error = true;
    }
  }
  if (error || !hasAddress) {
    return false;
  }
  return true;
};

export const cardInputs = [
  "card-name",
  "card-number",
  "card-cvc",
  "card-exp_month",
  "card-exp_year"
];

/**
 * Sync isDisable from helper with event required fields
 */

Template.Purchase.helpers({
  isDisabled: () => {
    const product = Session.get("selectedProduct");
    const hasAddress = Session.get("hasAddress");
    const splitPayment = Session.get("splitPayment");
    return isValid({ product, hasAddress, splitPayment, goal: true })
      ? {}
      : { disabled: "disabled" };
  },
  total: () => {
    const minZero = amount => {
      if (amount <= 0) {
        Session.set("noPayment", "si");
      } else {
        Session.set("noPayment", "no");
      }
      return amount;
    };
    const promoCode = Session.get("promoCode");
    const splitPayment = Session.get("splitPayment");
    const product = Session.get("selectedProduct");
    const resTotal = getPurchasePrice({
      product,
      promoCode,
      splitPayment: splitPayment && splitPayment === "si",
      giftCardAmount:
        Session.get("useGiftCard") === "si" && Clients.findOne()
          ? Clients.findOne().giftCardAmount
          : 0
    });
    return minZero(resTotal.finalPrice);
  },
  cantBuy: () =>
    Meteor.userId() && !Roles.userIsInRole(Meteor.userId(), "client"),
  redirectLogin: () => FlowRouter.go("login")
});

Template.Purchase.events({
  "click #pay-button": function() {
    event.preventDefault();
    const hasAddress = Session.get("hasAddress");
    const splitPayment = Session.get("splitPayment");
    const product = Session.get("selectedProduct");
    const treatmentGoal = $("#goal-input").val();
    const noPayment = Session.get("noPayment") === "si";
    const sameCard = Session.get("sameCard");
    const { last4 } = Clients.findOne() || {};
    let error = !isValid({
      product,
      hasAddress,
      splitPayment,
      goal: treatmentGoal
    });
    let date;
    if (!error) {
      if (!noPayment && (sameCard !== "si" || !last4)) {
        cardInputs.map(i => {
          if (!$(`#${i}`).val()) {
            error = true;
          }
        });
      }
      if (product.isTreatment) {
        date = times(product.numberOfSessions, index => {
          const value = $(`#datetimepicker-input-${index}`).val();
          if (!value) error = true;
          return value;
        });
      } else {
        date = $("#datetimepicker-input-0").val();
      }
    }

    // Check for address if is not user
    if (!Meteor.userId()) {
      if (hasAddress === "si") {
        addressFields.map(({ id }) => {
          if (!$(`#${id}`).val()) error = true;
        });
      }
      accountFields.map(({ id }) => {
        if (!$(`#${id}`).val()) error = true;
      });
    }

    if (error || !date) {
      Bert.alert("Por favor completa todos los campos de la orden", "danger");
      return;
    }

    if (!noPayment && (sameCard !== "si" || !last4)) {
      Session.set(
        "last4",
        $("#card-number")
          .val()
          .split("")
          .reverse()
          .slice(0, 4)
          .reverse()
          .join("")
      );
    }
    // Previene hacer submit más de una vez
    $("#pay-button").prop("disabled", true);
    if (!noPayment && (sameCard !== "si" || !last4)) {
      Conekta.Token.create(
        $("#credit-card-form"),
        conektaSuccessResponseHandler,
        conektaErrorResponseHandler
      );
    } else {
      conektaSuccessResponseHandler();
    }
    return false;
  }
});

const conektaSuccessResponseHandler = ({ id: token } = {}) => {
  const promoCode = Session.get("promoCode");
  const hasAddress = Session.get("hasAddress");
  const splitPayment = Session.get("splitPayment");
  const product = Session.get("selectedProduct");
  const treatmentGoal = $("#goal-input").val();
  const sameCard = Session.get("sameCard");
  const { last4: clientLast4 } = Clients.findOne() || {};
  const last4 = Session.get("last4");
  const useGiftCard = Session.get("useGiftCard") === "si";
  let date;
  let address;
  let userData;

  if (product.isTreatment) {
    date = times(product.numberOfSessions, index =>
      $(`#datetimepicker-input-${index}`).val()
    );
  } else {
    date = $("#datetimepicker-input-0").val();
  }

  // Check for address if is not user
  if (!Meteor.userId()) {
    if (hasAddress === "si")
      address = addressFields.reduce(
        (prev, { id }) => ({ ...prev, [id]: $(`#${id}`).val() }),
        {}
      );
    userData = accountFields.reduce(
      (prev, { id }) => ({ ...prev, [id]: $(`#${id}`).val() }),
      {}
    );
  }

  Session.set("last4", null);

  const orderSummary = [];
  const discountedPrice = Session.get("discountedPrice");
  const firstTimePrice = Session.get("firstPayment");
  orderSummary.push({ label: "Producto", value: product.title });
  if (splitPayment === "si") {
    orderSummary.push({
      label: "Precio total",
      value: prettyPrice(product.price),
      type: "price"
    });
    if (promoCode) {
      orderSummary.push({
        label: "Primer pago original",
        value: prettyPrice(firstTimePrice),
        type: "price"
      });
      orderSummary.push({
        label: "Descuento",
        value: prettyPrice(firstTimePrice - discountedPrice),
        type: "price"
      });
      orderSummary.push({
        label: "Hoy pagaste",
        value: prettyPrice(discountedPrice),
        type: "price"
      });
    } else {
      orderSummary.push({
        label: "Hoy pagaste",
        value: prettyPrice(firstTimePrice),
        type: "price"
      });
    }
  } else if (promoCode) {
    orderSummary.push({
      label: "Precio original",
      value: prettyPrice(product.price),
      type: "price"
    });
    orderSummary.push({
      label: "Descuento",
      value: prettyPrice(product.price - discountedPrice),
      type: "price"
    });
    orderSummary.push({
      label: "Precio Final",
      value: prettyPrice(discountedPrice),
      type: "price"
    });
  } else {
    orderSummary.push({
      label: "Precio",
      value: prettyPrice(product.price),
      type: "price"
    });
  }
  if (product.isTreatment) {
    date.map((date, index) => {
      orderSummary.push({
        label: `Sesión ${index + 1}`,
        value: date,
        type: "date"
      });
    });
  } else {
    orderSummary.push({
      label: "Fecha",
      value: date,
      type: "date"
    });
  }
  if (userData) {
    orderSummary.push({
      label: "Nombre",
      value: `${userData.firstname} ${userData.lastname}`
    });
    orderSummary.push({
      label: "Email",
      value: `${userData.email}`
    });
  }
  if (hasAddress === "si") {
    const location = Locations.findOne(address.locationId) || {};
    orderSummary.push({
      label: "Dirección",
      value: `${address.street1} ${address.street2} ${address.zip}, ${
        location.name
      }. ${address.reference}`
    });
  } else {
    orderSummary.push({
      label: "Dirección",
      isLink: true,
      value: "https://goo.gl/maps/U3XJH8jhWuT2"
    });
  }
  Session.set("orderSummary", orderSummary);

  Meteor.call(
    "purchase",
    {
      product: product.slug,
      promoCodeId: promoCode ? promoCode._id : "",
      date,
      hasAddress: hasAddress === "si",
      splitPayment: splitPayment === "si",
      useGiftCard,
      userId: Meteor.userId(),
      treatmentGoal,
      creditCardInfo: {
        token,
        last4,
        sameCard: sameCard === "si" && clientLast4
      },
      address,
      userData,
      orderSummary
    },
    err => {
      if (err) {
        Bert.alert(err.reason, "danger");
        $("#pay-button").prop("disabled", false);
      } else {
        Bert.alert("¡Felicidades! Tu orden ha sido exitosa", "success");
        if (Meteor.userId()) {
          FlowRouter.go("client-profile");
        } else {
          FlowRouter.go("success-purchase");
        }
      }
    }
  );
};

const conektaErrorResponseHandler = response => {
  Bert.alert(response.message_to_purchaser, "danger");
  $("#pay-button").prop("disabled", false);
};
