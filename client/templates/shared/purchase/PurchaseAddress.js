import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { Session } from "meteor/session";
import { addressFields } from "../../../../common/businessLogic";

Template.PurchaseAddress.helpers({
  clientHasNoAdresss: () => {
    if (Meteor.userId() && Clients.findOne()) {
      return !Clients.findOne().hasAddress;
    }
    return false;
  },
  clientDisabledAddress: () => {
    if (Meteor.userId() && Clients.findOne() && !Clients.findOne().hasAddress) {
      return { disabled: "disabled" };
    }
    return {};
  },
  addressFields: () => addressFields,
  hideLocation: () => {
    const locations = Locations.find();
    return locations.count() <= 1;
  }
});

const removePromoCode = () => {
  if (Session.get("promoCode")) {
    Bert.alert(
      "Por seguridad, tu código de promoción ha sido eliminado. Ingresalo de nuevo",
      "danger"
    );
    Session.set("promoCode", null);
  }
};

Template.PurchaseAddress.events({
  "click #yes-button": function() {
    event.preventDefault();
    Session.set("hasAddress", "si");
    removePromoCode();
  },
  "click #no-button": function() {
    event.preventDefault();
    Session.set("hasAddress", "no");
    removePromoCode();
  }
});
