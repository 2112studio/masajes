import { Template } from "meteor/templating";

Template.PurchaseDate.onRendered(() => {
  $("#datetimepicker-0").datetimepicker({ useCurrent: true });
  [1, 2, 3, 4, 5, 6].map(i => {
    $(`#datetimepicker-${i}`).datetimepicker({
      date: moment().add(6 * i + 6, "d")
    });
    $(`#datetimepicker-${i - 1}`).on("change.datetimepicker", function(e) {
      $(`#datetimepicker-${i}`).datetimepicker("minDate", e.date.add(6, "d"));
    });
  });
});

Template.PurchaseDate.helpers({
  numberOfDates: () => {
    const product = Session.get("selectedProduct");
    if (product && product.isTreatment) return product.numberOfSessions;
    return 1;
  }
});
