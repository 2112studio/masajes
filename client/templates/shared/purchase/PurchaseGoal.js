import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.PurchaseGoal.helpers({
  show: () => {
    const product = Session.get("selectedProduct");
    return product && product.isTreatment;
  }
});
