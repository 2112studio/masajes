import { Template } from "meteor/templating";

Template.PurchaseProductSelectItem.events({
  "click #product-button": function() {
    event.preventDefault();
    Session.set("selectedProduct", this);
    if (this.isTreatment && !Session.get("splitPayment")) {
      Session.set("splitPayment", "no");
    }
  }
});
