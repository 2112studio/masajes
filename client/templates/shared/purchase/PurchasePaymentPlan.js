import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.PurchasePaymentPlan.events({
  "click #yes-plan-button": function() {
    event.preventDefault();
    Session.set("splitPayment", "si");
  },
  "click #no-plan-button": function() {
    event.preventDefault();
    Session.set("splitPayment", "no");
  }
});
