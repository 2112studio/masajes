import { Template } from "meteor/templating";
import { accountFields } from "../../../../common/businessLogic";

Template.PurchaseUser.helpers({
  accountFields: () => accountFields
});
