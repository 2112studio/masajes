import { Template } from "meteor/templating";

Template.PurchaseGiftCard.events({
  "click #yes-gift-button": function() {
    event.preventDefault();
    Session.set("useGiftCard", "si");
  },
  "click #no-gift-button": function() {
    event.preventDefault();
    Session.set("useGiftCard", "no");
  }
});
