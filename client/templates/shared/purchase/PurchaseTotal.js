import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.PurchaseTotal.helpers({
  appliedPromo: () => Session.get("promoCode"),
  splitPayments: product => {
    const splitPayment = Session.get("splitPayment");
    return product.isTreatment && splitPayment && splitPayment === "si";
  },
  splitPaymentValidDiscount: promoCode =>
    promoCode && promoCode.type !== "newClients",
  splitPaymentValidDiscountAmount: (amount, promoCode) => {
    let total = amount;
    switch (promoCode.type) {
      case "amount":
        total = amount - promoCode.amount;
        break;
      case "percentage":
        total = amount * (1 - promoCode.amount * 0.01);
        break;
    }
    total = total < 0 ? 0 : total;
    // Set order summary prices, to avoid recalculation
    Session.set("discountedPrice", total);
    return total;
  },
  discountedPrice: product => {
    const promoCode = Session.get("promoCode");
    let total = product.price;
    switch (promoCode.type) {
      case "newClients":
        total = product.firstTimePrice;
        break;
      case "amount":
        total = product.price - promoCode.amount;
        break;
      case "percentage":
        total = product.price * (1 - promoCode.amount * 0.01);
        break;
    }

    total = total < 0 ? 0 : total;
    // Set order summary prices, to avoid recalculation
    Session.set("discountedPrice", total);
    return total;
  },
  firstPayment: ({ partialPayments, price }) => {
    const payment = partialPayments[0] * price;
    // Set order summary prices, to avoid recalculation
    Session.set("firstPayment", payment);
    return payment;
  },
  removePromoCode: () => {
    Session.set("promoCode", null);
  }
});
