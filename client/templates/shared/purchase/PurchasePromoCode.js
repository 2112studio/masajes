import { Template } from "meteor/templating";
import { Meteor } from "meteor/meteor";
import { Session } from "meteor/session";

Template.PurchasePromoCode.events({
  "click #validate-promo-code": () =>
    Meteor.call(
      "validatePromoCode",
      {
        code: $("#promo-code-input").val(),
        isNewUser: !Meteor.userId(),
        locationId: $("#locationId").val()
      },
      (err, res) => {
        if (err) {
          Bert.alert(`Lo sentimos. ${err.reason}`, "danger");
          Session.set("promoCode", null);
        } else {
          Session.set("promoCode", res);
          Bert.alert("¡Felicidades! Tu descuento ha sido aplicado", "success");
        }
      }
    )
});
