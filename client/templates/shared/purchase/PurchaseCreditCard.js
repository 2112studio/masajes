import { Template } from "meteor/templating";
import { Session } from "meteor/session";

Template.PurchaseCreditCard.onRendered(() => {
  if (!Session.get("sameCard")) {
    Session.set("sameCard", "si");
  }
});

Template.PurchaseCreditCard.events({
  "click #yes-card-button": function() {
    event.preventDefault();
    Session.set("sameCard", "si");
  },
  "click #no-card-button": function() {
    event.preventDefault();
    Session.set("sameCard", "no");
  }
});
