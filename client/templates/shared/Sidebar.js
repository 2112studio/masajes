import { Template } from "meteor/templating";

Template.Sidebar.events({
  "click #logoutButton": () => {
    AccountsTemplates.logout();
  }
});
