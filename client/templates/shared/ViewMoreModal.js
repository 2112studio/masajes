import { Template } from "meteor/templating";

Template.ViewMoreModal.helpers({
  id: function() {
    if (this.id) return this.id;
    return "view-more-modal";
  },
  data: function() {
    return this.data && this.data.map(i => ({ ...i, full: this.full }));
  }
});
