import { Template } from "meteor/templating";

Template.EditButton.events({
  "click #edit-button": function() {
    Session.set("editId", this.id);
    Session.set("editMode", 1);
    Meteor.setTimeout(function() {
      $("#edit-modal").modal("show");
    }, 500);
  }
});
