import { Template } from "meteor/templating";
import { pastStatus, pendingStatus } from "../../../../common/businessLogic";
import Clients, { clientQuestions } from "../../../../collections/Clients";
import products from "../../../../common/products";

Template.PatientHistory.onCreated(function() {
  this.autorun(() => {
    // First try to get client userId from param
    let clientUserId = FlowRouter.getParam("id");
    if (clientUserId) {
      // If param is set, then we need to subscribe to the client info
      this.subscribe("clientById", clientUserId);
    } else {
      // If param is not set, it means it's being accesed by the user
      // We don't subscribe to user because it's already subscribed on sidebar
      clientUserId = Meteor.userId();
    }
    // Subscribe to all client sessions
    this.subscribe("clientSessions", clientUserId);
    this.subscribe("clientSessionsTherapists", clientUserId);
  });
});

Template.PatientHistory.helpers({
  noSessions: () => !Sessions.find().count(),
  currentTreatment: () => {
    const client = Clients.findOne();
    if (client) return client.treatments.find(({ isCurrent }) => isCurrent);
  },
  currentTreatmentSessions: () =>
    Sessions.find(
      {
        _id: {
          $in: Clients.findOne().treatments.find(({ isCurrent }) => isCurrent)
            .sessions
        }
      },
      { sort: { scheduledDate: 1 } }
    ),
  pastTreatments: () => {
    const client = Clients.findOne();
    if (client) {
      // Create a hash to store each treatment count so far. That will help to identify each treatment among other of same type
      const hash = {};
      const indexedTreatments = client.treatments.map(i => {
        if (!hash[i.type]) {
          hash[i.type] = 0;
        }
        const toReturn = { ...i, customIndex: hash[i.type] };
        hash[i.type]++;
        return toReturn;
      });
      const treatments = indexedTreatments.filter(
        ({ isCurrent }) => !isCurrent
      );
      if (treatments) return treatments;
    }
  },
  pastTreatmentSessions: index =>
    Sessions.find(
      {
        _id: {
          $in: Clients.findOne().treatments.filter(
            ({ isCurrent }) => !isCurrent
          )[index].sessions
        }
      },
      { sort: { scheduledDate: 1 } }
    ),
  pastNoTreatmentSessions: () => {
    const sessions = Sessions.find(
      { status: { $in: pastStatus }, isTreatment: false },
      { sort: { scheduledDate: 1 } }
    );
    if (sessions.count()) return sessions;
  },
  pendingNoTreatmentSessions: () => {
    const sessions = Sessions.find(
      { status: { $in: pendingStatus }, isTreatment: false },
      { sort: { scheduledDate: 1 } }
    );
    if (sessions.count()) return sessions;
  },
  viewProfileData: () => {
    const client = Clients.findOne();
    if (!client) return [];
    return [
      {
        title: "Perfil",
        items: [
          ...clientQuestions.map(({ question, slug }) => ({
            label: question,
            value: (client.profile && client.profile[`question${slug}`]) || "-"
          })),
          {
            label: "Fecha de solicitud",
            value: moment(client.createdAt).format("ll")
          },
          {
            label: "Última modificación",
            value: moment(client.updatedAt).format("ll")
          }
        ]
      }
    ];
  },
  currentTreatmentName: () => {
    const { treatments } = Clients.findOne();
    const { type } = treatments.find(({ isCurrent }) => isCurrent) || {};
    const product = products.find(({ slug }) => slug === type);
    return product.sessionNames[
      treatments.filter(({ type: tType }) => tType === type).length - 1
    ];
  },
  pastTreatmentName: (type, index) => {
    const product = products.find(({ slug }) => slug === type);
    return product.sessionNames[index];
  }
});

Template.PatientHistory.events({
  "click #view-profile-button": function() {
    Session.set("viewMoreId", this._id);
    Meteor.setTimeout(function() {
      $("#view-profile-modal").modal("show");
    }, 500);
  },
  "click #add-cash-button": () =>
    Meteor.setTimeout(function() {
      $("#add-cash-modal").modal("show");
    }, 500),
  "click #cancel-treatment-button": () =>
    swal(
      {
        title: "¿Estás seguro?",
        text: "No se pude deshacer esta acción",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E64759",
        confirmButtonText: "Sí, ¡Cancelar!"
      },
      () => {
        const { treatments, _id } = Clients.findOne();
        const treatmentIndex = treatments.findIndex(
          ({ isCurrent }) => isCurrent
        );
        Meteor.call("cancelTreatment", treatmentIndex, _id, err => {
          if (err) {
            console.log(err);
            Bert.alert(err.reason, "danger", "growl-top-right");
          } else {
            Bert.alert(
              "El circuito se ha cancelado",
              "success",
              "growl-top-right"
            );
          }
        });
      }
    )
});
