import { Template } from "meteor/templating";
import { genericAdminError } from "../../../general";

Template.PatientAddCash.events({
  "click #add-cash-save-button"() {
    const treatmentIndex = Clients.findOne().treatments.findIndex(
      ({ isCurrent }) => isCurrent
    );
    Meteor.call(
      "addCashTreatment",
      treatmentIndex,
      this.id,
      $("#add-cash-input").val(),
      err => {
        if (err) {
          genericAdminError();
        } else {
          Bert.alert("El pago ha sido agregado", "success");
          $("#add-cash-modal").modal("hide");
        }
      }
    );
  }
});
