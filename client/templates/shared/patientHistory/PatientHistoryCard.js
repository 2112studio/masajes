import { Template } from "meteor/templating";
import { surveyQuestions } from "../../../../common/businessLogic";

Template.PatientHistoryCard.helpers({
  checkInQuestions() {
    return surveyQuestions.map(({ label, slug }) => ({
      title: label,
      value: this.checkInSurvey[slug]
    }));
  },
  checkOutQuestions() {
    return surveyQuestions.map(({ label, slug }) => ({
      title: label,
      value: this.checkOutSurvey[slug]
    }));
  }
});
