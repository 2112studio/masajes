import { Template } from "meteor/templating";
import { therapistsQuestions } from "../../../collections/Therapists";
import times from "lodash.times";

Template.TherapistInfoButton.helpers({
  viewProfileData() {
    if (!this.therapist) return [];
    return [
      {
        title: "Datos Personales",
        items: [
          {
            label: "Nombre",
            value: `${this.therapist.firstname} ${this.therapist.lastname}`
          },
          {
            label: "Email",
            value: this.therapist.email
          },
          ...times(3, index => {
            const { question, slug } = therapistsQuestions[index];
            return {
              label: question,
              value:
                (this.therapist.profile &&
                  this.therapist.profile[`question${slug}`]) ||
                "-" ||
                "-"
            };
          })
        ]
      },
      {
        title: "Perfil",
        items: [
          ...times(therapistsQuestions.length - 3, index => {
            index += 3;
            const { question, slug } = therapistsQuestions[index];
            return {
              label: question,
              value:
                (this.therapist.profile &&
                  this.therapist.profile[`question${slug}`]) ||
                "-" ||
                "-"
            };
          }),
          {
            label:
              "¿En qué zona de la ciudad estas dispuesto a impartir masajes? (Marca los que sean necesarios)",
            value:
              (this.therapist.locationsId &&
                this.therapist.locationsId.map(
                  _id =>
                    (Locations.findOne({ _id })
                      ? Locations.findOne({ _id })
                      : {}
                    ).name
                )) ||
              ""
          },
          {
            label: "Fecha de solicitud",
            value: moment(this.therapist.createdAt).format("ll")
          },
          {
            label: "Última modificación",
            value: moment(this.therapist.updatedAt).format("ll")
          }
        ]
      }
    ];
  }
});

Template.TherapistInfoButton.events({
  "click #view-therapist-profile-button": function() {
    Meteor.setTimeout(function() {
      $("#view-therapist-profile-modal").modal("show");
    }, 500);
  }
});
