import { Template } from "meteor/templating";

Template.AddressButton.helpers({
  viewAddressData: function() {
    if (!this.hasAddress || !this.address) return [];
    return [
      {
        title: "Dirección",
        items: [
          { label: "Calle y Número", value: this.address.street1 },
          { label: "Colonia", value: this.address.street2 },
          { label: "Código Postal", value: this.address.zip },
          { label: "Referencias", value: this.address.reference }
        ]
      }
    ];
  }
});

Template.AddressButton.events({
  "click #view-address-button": function() {
    Meteor.setTimeout(function() {
      $("#view-address-modal").modal("show");
    }, 500);
  }
});
