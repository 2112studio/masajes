import { Template } from "meteor/templating";

Template.PendingConfirmation.helpers({
  isAdmin: () =>
    Roles.userIsInRole(Meteor.userId(), "admin") ||
    Roles.userIsInRole(Meteor.userId(), "editor")
});
