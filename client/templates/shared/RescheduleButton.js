import { Template } from "meteor/templating";
import { genericAdminError } from "../../general";

Template.RescheduleButton.events({
  "click #open-reschedule-button": () =>
    Meteor.setTimeout(function() {
      $("#reschedule-modal").modal("show");
    }, 500),
  "click #reschedule-button"() {
    const date = $("#datetimepicker-input").val();
    if (!date) return;
    const parsedDate = moment(date, "DD/MM/YYYY k:mm").toISOString();
    Meteor.call("rescheduleSession", this.id, parsedDate, err => {
      if (err) {
        genericAdminError();
      } else {
        if (Roles.userIsInRole(Meteor.userId(), "client")) {
          Bert.alert(
            "Se ha procesado tu solicitud de cambio. En breve te contactaremos",
            "success"
          );
        } else {
          Bert.alert(
            "Se ha reagendado la sesión",
            "success",
            "growl-top-right"
          );
        }
      }
      $("#reschedule-modal").modal("hide");
    });
  }
});
