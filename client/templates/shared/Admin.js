import { Template } from "meteor/templating";

Template.Admin.helpers({
  goToAdmin: () => FlowRouter.go("admin-pending-sessions"),
  goToClient: () => FlowRouter.go("client-profile"),
  goToTherapist: () => FlowRouter.go("therapist-pending-sessions"),
  goToPending: () => FlowRouter.go("pending-confirmation"),
  goToLogin: () => FlowRouter.go("signin")
});
