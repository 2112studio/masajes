import { Template } from "meteor/templating";

Template.RemoveButton.events({
  "click #confirm-remove": event =>
    swal(
      {
        title: "¿Estás seguro?",
        text: "No se pude deshacer esta acción",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#E64759",
        confirmButtonText: "Sí, ¡Borrar!"
      },
      () =>
        $(event.target)
          .parents("#remove-button-container")
          .find(".delete-btn")
          .trigger("click")
    )
});
