// We identified the templates that need to be overridden by looking at the available templates
// here: https://github.com/meteor-useraccounts/unstyled/tree/master/lib

Template["LoginInputOverride"].replaces("atTextInput");
Template["LoginPwdButtonOverride"].replaces("atPwdFormBtn");
