import "./helpers";
import "./general";

const accountsTranslationsES = {
  firstname: "Nombre(s)",
  lastname: "Apellido(s)",
  "Required Field": "Campo Requerido",
  "Minimum required length: 3": "Mínimo 3 caractéres",
  "Minimum required length: 6": "Mínimo 6 caractéres",
  "Invalid email": "Email inválido",
  forgotPassword: "¿Olvidaste tu contraseña",
  signUp: "¡Regístrate!",
  error: {
    accounts: {
      "Acceso denegado": "Acceso denegado. Intenta de nuevo"
    }
  },
  clickAgree: "Al registrarse aprueba la",
  privacyPolicy: "Política de Privacidad",
  terms: "Términos y Condiciones"
};

Meteor.startup(function() {
  T9n.setLanguage("es");
  T9n.map("es", accountsTranslationsES);
});

$(document).ready(() => {
  $.tablesorter.addParser({
    id: "customDate",
    is: () => 0,
    format: s => moment(s, "DD/MM/YY").valueOf(),
    type: "numeric"
  });
  $.tablesorter.addParser({
    id: "customDatetime",
    is: () => 0,
    format: s => moment(s, "llll").valueOf(),
    type: "numeric"
  });

  // Set preferences of datetimepicker
  $.fn.datetimepicker.Constructor.Default = {
    ...$.fn.datetimepicker.Constructor.Default,
    locale: "es",
    daysOfWeekDisabled: [0, 6],
    minDate: moment().add(2, "days"),
    disabledHours: [0, 1, 2, 3, 4, 5, 6, 7, 8, 18, 19, 20, 21, 22, 23, 24]
  };

  Meteor.call("getConektaKey", (_, res) => Conekta.setPublicKey(res));

  easeScroll = target => {
    let topPos = -60;
    topPos += $(target).offset().top;
    $("html,body").animate({ scrollTop: topPos }, 1000, "easeInOutQuart");
    return false;
  };

  scrollTop = () =>
    $("html,body").animate({ scrollTop: 0 }, 0, "easeInOutQuart");

  // Eased scroll links. Verify if they come from the same page or from external route
  $("body").on("click", ".scroll-to", function() {
    const href = $(this).attr("href");
    let target;
    if (href.charAt(0) === "#" || href.split("#").length > 1) {
      target = href.charAt(0) === "#" ? href : `#${href.split("#")[1]}`;
      if (FlowRouter.current().path !== "/") {
        return;
      }
    } else {
      target = `#${href.split("/")[2]}`;
    }
    easeScroll(target);
  });

  $("body").on("click", ".scroll-top", scrollTop);
});
