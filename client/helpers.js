import { Template } from "meteor/templating";
import { FlowRouter } from "meteor/kadira:flow-router";
import products from "../common/products";
import { pastStatus, pendingStatus, statusDict } from "../common/businessLogic";

// General
Template.registerHelper("redirectToAdmin", () => {
  FlowRouter.go("admin");
});
Template.registerHelper("editId", () => Session.get("editId"));
Template.registerHelper("humanDate", date => moment(date).format("DD/MM/YY"));
Template.registerHelper("humanDatetime", date => moment(date).format("llll"));
Template.registerHelper("humanDatetimeF", (date, format) =>
  moment(date, format).format("llll")
);
Template.registerHelper("humanTime", date => moment(date).format("H:mm"));
Template.registerHelper("userInRole", (id, role) =>
  Roles.userIsInRole(id, role)
);
Template.registerHelper("yesNo", condition => (condition ? "Si" : "No"));
Template.registerHelper("yesNoColor", condition =>
  condition
    ? `<span class="text-success">Si</span>`
    : `<span class="text-danger">No</span>`
);
export const prettyPrice = (amount = 0) =>
  amount.toLocaleString("en-US", { style: "currency", currency: "USD" });
Template.registerHelper("prettyPrice", prettyPrice);
Template.registerHelper("canSeeHistory", () =>
  Roles.userIsInRole(Meteor.userId(), ["admin", "client", "therapist"])
);
Template.registerHelper(
  "isNotClient",
  () => !Roles.userIsInRole(Meteor.userId(), "client")
);
Template.registerHelper(
  "isNotTherapist",
  () => !Roles.userIsInRole(Meteor.userId(), "therapist")
);
export const needsPayment = (
  sessionId,
  { price, partialPayments, partialPaymentsSessions },
  { sessions, amountPaid }
) => {
  const sessionNumber = sessions.findIndex(i => i === sessionId) + 1;
  // Get index of next payment
  const nextPaymentIndex = partialPaymentsSessions.findIndex(
    i =>
      i ===
      partialPaymentsSessions.reduce(
        (prev, curr) => (curr <= sessionNumber ? curr : prev),
        -1
      )
  );
  // Add up the total percentage that should have been paid, and multiply with price
  const amountDue =
    partialPayments.reduce(
      (prev, curr, index) => (index <= nextPaymentIndex ? prev + curr : prev),
      0
    ) * price;
  return amountPaid < amountDue;
};

// Products
Template.registerHelper("products", () => products);
Template.registerHelper("selectedProduct", () =>
  Session.get("selectedProduct")
);
Template.registerHelper("prettySlug", type => {
  const { title } = products.find(({ slug }) => slug === type) || {};
  return title;
});

// PromoCodes
Template.registerHelper("prettyPromoCodeType", type => {
  const { label } =
    [
      { value: "amount", label: "Cantidad" },
      { value: "percentage", label: "Porcentaje" },
      { value: "newClients", label: "Nuevos Clientes" }
    ].find(({ value }) => value === type) || {};
  return label;
});

// Locations
Template.registerHelper("locationsNames", locations =>
  locations.map(_id => Locations.findOne({ _id }).name)
);
Template.registerHelper("locationName", _id => Locations.findOne({ _id }).name);

Template.registerHelper("prettyPromoCodeAmount", (type, amount) => {
  switch (type) {
    case "amount":
      return amount.toLocaleString("en-US", {
        style: "currency",
        currency: "USD"
      });
    case "percentage":
      return `${amount}%`;
    default:
      return "";
  }
});

// Sessions
Template.registerHelper("sessionPatient", function() {
  const session = Sessions.findOne(this._id);
  return Clients.findOne(session.clientId);
});
Template.registerHelper("sessionTherapist", function() {
  const session = Sessions.findOne(this._id);
  return Therapists.findOne(session.therapistId);
});
Template.registerHelper("prettyStatus", status => {
  const statusFound = statusDict.find(({ value }) => value === status) || {};
  return statusFound.label;
});
Template.registerHelper("pendingSessions", () =>
  Sessions.find({ status: { $in: pendingStatus } })
);
Template.registerHelper("pastSessions", () =>
  Sessions.find({ status: { $in: pastStatus } })
);

// Client profile
Template.registerHelper("currentTreatment", () =>
  Clients.findOne().treatments.find(({ isCurrent }) => isCurrent)
);
Template.registerHelper("currentTreatmentProduct", () => {
  const { type } =
    Clients.findOne().treatments.find(({ isCurrent }) => isCurrent) || {};
  return products.find(({ slug }) => slug === type);
});

/**
 * Sort 1 means ascending. This assures that session info is always the first scheduled
 * in case there are two overlaping sessions
 */
Template.registerHelper("readySession", () =>
  Sessions.findOne({ status: "ready" }, { sort: { scheduledDate: 1 } })
);
Template.registerHelper("ongoingSession", () =>
  Sessions.findOne({ status: "ongoing" }, { sort: { scheduledDate: 1 } })
);
Template.registerHelper("readySessionPatient", () => {
  const session =
    Sessions.findOne({ status: "ready" }, { sort: { scheduledDate: 1 } }) || {};
  return Clients.findOne({ _id: session.clientId });
});
Template.registerHelper("ongoingSessionPatient", () => {
  const session =
    Sessions.findOne({ status: "ongoing" }, { sort: { scheduledDate: 1 } }) ||
    {};
  return Clients.findOne({ _id: session.clientId });
});

// Collections
Template.registerHelper("users", () => Meteor.users.find());
Template.registerHelper("clients", () => Clients.find());
Template.registerHelper("therapists", () => Therapists.find());
Template.registerHelper("prospects", () => Prospects.find());
Template.registerHelper("locations", () => Locations.find());
Template.registerHelper("promoCodes", () => PromoCodes.find());
Template.registerHelper("sessions", () => Sessions.find());
Template.registerHelper("posts", () => Posts.find());

Template.registerHelper("user", () => Meteor.users.findOne());
Template.registerHelper("client", () => Clients.findOne());
Template.registerHelper("therapist", () => Therapists.findOne());
Template.registerHelper("prospect", () => Prospects.findOne());
Template.registerHelper("location", () => Locations.findOne());
Template.registerHelper("promoCode", () => PromoCodes.findOne());
Template.registerHelper("session", () => Sessions.findOne());
Template.registerHelper("post", () => Posts.findOne());
