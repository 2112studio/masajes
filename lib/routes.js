import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";
////////////////////
/// Public routes
////////////////////

FlowRouter.route("/", {
  name: "home",
  action() {
    BlazeLayout.render("LandingLayout", { main: "Home" });
  }
});

FlowRouter.route("/pbi", {
  name: "pbi",
  action() {
    BlazeLayout.render("LandingLayout", { main: "PBI" });
  }
});

FlowRouter.route("/cuerpomente", {
  name: "cuerpomente",
  action() {
    BlazeLayout.render("LandingLayout", { main: "CuerpoMente" });
  }
});

FlowRouter.route("/bienestarcorporal", {
  name: "corporal",
  action() {
    BlazeLayout.render("LandingLayout", { main: "BienestarCorporal" });
  }
});

FlowRouter.route("/unete", {
  name: "careers",
  action() {
    BlazeLayout.render("LandingLayout", { main: "Careers" });
  }
});

FlowRouter.route("/registroExitoso", {
  name: "careers-success",
  action() {
    BlazeLayout.render("LandingLayout", { main: "CareersSuccess" });
  }
});

FlowRouter.route("/privacidad", {
  name: "privacy",
  action() {
    BlazeLayout.render("LandingLayout", { main: "Privacy" });
  }
});

FlowRouter.route("/reservar", {
  name: "purchase",
  action() {
    BlazeLayout.render("LandingLayout", { main: "LandingPurchase" });
  }
});

FlowRouter.route("/certificadosderegalo", {
  name: "gift-card",
  action() {
    BlazeLayout.render("LandingLayout", { main: "LandingGiftCard" });
  }
});

FlowRouter.route("/compraExitosa", {
  name: "success-purchase",
  action() {
    BlazeLayout.render("LandingLayout", { main: "SuccessPurchase" });
  }
});

FlowRouter.route("/blog", {
  name: "blog",
  action() {
    BlazeLayout.render("LandingLayout", { main: "BlogHome" });
  }
});

FlowRouter.route("/blog/:slug", {
  name: "blog-post",
  action() {
    BlazeLayout.render("LandingLayout", { main: "BlogPost" });
  }
});

////////////////////
/// Admin routes
////////////////////

FlowRouter.route("/admin", {
  name: "admin",
  action() {
    BlazeLayout.render("Admin");
  }
});

FlowRouter.route("/admin/historial", {
  name: "admin-past-sessions",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminPastSessions" });
  }
});

FlowRouter.route("/admin/asignar", {
  name: "admin-assign-sessions",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminAssignSessions" });
  }
});

FlowRouter.route("/admin/pendientes", {
  name: "admin-pending-sessions",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminPendingSessions" });
  }
});

FlowRouter.route("/admin/clientes", {
  name: "admin-clients",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminClients" });
  }
});

FlowRouter.route("/admin/promos", {
  name: "admin-promo-codes",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminPromoCodes" });
  }
});

FlowRouter.route("/admin/ciudades", {
  name: "admin-locations",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminLocations" });
  }
});

FlowRouter.route("/admin/admins", {
  name: "admin-admins",
  action() {
    BlazeLayout.render("AdminLayout", {
      main: "AdminAdmins",
      allowedFor: "superadmin"
    });
  }
});

FlowRouter.route("/admin/terapeutas", {
  name: "admin-therapists",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminCurrentTherapists" });
  }
});

FlowRouter.route("/admin/solicitudes", {
  name: "admin-therapists-requests",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminTherapistsRequests" });
  }
});

FlowRouter.route("/admin/prospectos", {
  name: "admin-therapists-prospects",
  action() {
    BlazeLayout.render("AdminLayout", { main: "AdminProspectsRequests" });
  }
});

FlowRouter.route("/admin/cliente/:id", {
  name: "admin-patient-history",
  action() {
    BlazeLayout.render("AdminLayout", { main: "PatientHistory" });
  }
});

////////////////////
/// Editor routes
////////////////////
FlowRouter.route("/editor/posts", {
  name: "editor-posts",
  action() {
    BlazeLayout.render("AdminLayout", {
      main: "EditorBlogPosts",
      allowedFor: "editor"
    });
  }
});

FlowRouter.route("/editor/nuevoPost", {
  name: "editor-new-post",
  action() {
    BlazeLayout.render("AdminLayout", {
      main: "EditorNewPost",
      allowedFor: "editor"
    });
  }
});

FlowRouter.route("/editor/post/:id", {
  name: "editor-edit-post",
  action() {
    BlazeLayout.render("AdminLayout", {
      main: "EditorEditPost",
      allowedFor: "editor"
    });
  }
});

////////////////////
/// Client routes
////////////////////
FlowRouter.route("/cliente/miperfil", {
  name: "client-profile",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientProfile" });
  }
});

FlowRouter.route("/cliente/historial", {
  name: "client-history",
  action() {
    BlazeLayout.render("ClientLayout", { main: "PatientHistory" });
  }
});

FlowRouter.route("/cliente/comprar", {
  name: "client-purchase",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientPurchase" });
  }
});

FlowRouter.route("/cliente/micodigo", {
  name: "client-promo-code",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientPromoCode" });
  }
});

FlowRouter.route("/cliente/misdatos", {
  name: "client-user-profile",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientUserProfile" });
  }
});

FlowRouter.route("/cliente/certificados", {
  name: "client-gift",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientGiftCard" });
  }
});

FlowRouter.route("/cliente/acceso", {
  name: "request-access",
  action() {
    BlazeLayout.render("ClientLayout", { main: "ClientRequestAccess" });
  }
});

FlowRouter.route("/pendiente", {
  name: "pending-confirmation",
  action() {
    BlazeLayout.render("PendingConfirmation");
  }
});

////////////////////
/// Therapist routes
////////////////////

FlowRouter.route("/terapeuta/pendientes", {
  name: "therapist-pending-sessions",
  action() {
    BlazeLayout.render("TherapistLayout", { main: "TherapistPendingSessions" });
  }
});

FlowRouter.route("/terapeuta/historial", {
  name: "therapist-past-sessions",
  action() {
    BlazeLayout.render("TherapistLayout", { main: "TherapistPastSessions" });
  }
});

FlowRouter.route("/terapeuta/cliente/:id", {
  name: "therapist-patient-history",
  action() {
    BlazeLayout.render("TherapistLayout", { main: "PatientHistory" });
  }
});
////////////////////
/// Login hookers
////////////////////
Accounts.onLogout(() =>
  // Hack: Without setTimeout it doesn't change routes
  setTimeout(() => {
    FlowRouter.go("signin");
  }, 300)
);
