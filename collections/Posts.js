import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";

SimpleSchema.extendOptions(["autoform", "denyInsert", "denyUpdate"]);

// Adds a collection for posts
Posts = new Mongo.Collection("posts");

Posts.allow({
  insert: userId => Roles.userIsInRole(userId, ["admin", "editor"]),
  update: userId => Roles.userIsInRole(userId, ["admin", "editor"]),
  remove: userId => Roles.userIsInRole(userId, ["admin", "editor"])
});

// Create slug from string
const slugify = text =>
  text
    .toString()
    .toLowerCase()
    .replace(/á/g, "a")
    .replace(/é/g, "e")
    .replace(/í/g, "i")
    .replace(/ó/g, "o")
    .replace(/ú/g, "u")
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(/[^\w\-]+/g, "") // Remove all non-word chars
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text

// Posts schema
const PostsSchema = new SimpleSchema({
  title: String,
  body: String,
  description: String,
  thumbnail: String,
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return {
          $setOnInsert: new Date()
        };
      } else {
        this.unset(); // Prevent user from supplying their own value
      }
    },
    autoform: {
      omit: true
    }
  },
  views: {
    type: Number,
    defaultValue: 0,
    autoform: {
      omit: true
    }
  },
  slug: {
    type: String,
    autoValue: function() {
      var content = this.field("title");
      if (content.isSet) {
        return slugify(this.field("title").value);
      }
    },
    autoform: {
      omit: true
    }
  }
});

Posts.attachSchema(PostsSchema);

export default Posts;

Posts.sorted = ({ selector = {}, limit, sortBy = "createdAt", desc }) =>
  Posts.find(selector, {
    sort: {
      [sortBy]: desc ? 1 : -1
    },
    limit
  });
