import { Meteor } from "meteor/meteor";

Meteor.users.allow({
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});
