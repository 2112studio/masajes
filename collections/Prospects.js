import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import SimpleSchema from "simpl-schema";
import { getSchemaQuestions } from "./helpers";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

class ProspectsCollection extends Mongo.Collection {
  insert(doc, callback) {
    if (Meteor.isClient) {
      Bert.alert("¡Felicidades! Tu registro ha sido existoso", "success");
      FlowRouter.go("careers-success");
    }
    return super.insert(doc, callback);
  }
}

Prospects = new ProspectsCollection("prospects");

Prospects.allow({
  insert: () => true,
  update: userId => Roles.userIsInRole(userId, ["admin"]),
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

export const denyUpdateFields = ["firstname", "lastname", "email"];

export const prospectsQuestions = [
  {
    question: "Género",
    options: ["Femenino", "Masculino"],
    slug: "gender",
    required: true
  },
  { question: "Fecha de nacimiento", slug: "birthday", required: true },
  { question: "Teléfono de contacto", slug: "phone", required: true },
  {
    question: "¿Cómo te enteraste de esta oportunidad de estudio y trabajo?",
    slug: "findout",
    required: true
  },
  {
    question: "¿Actualmente eres terapeuta corporal?",
    options: ["Sí", "No"],
    slug: "currentTherapist",
    required: true
  },
  {
    question:
      "Universidad/escuela en donde adquiriste o adquieres tus estudios",
    slug: "education",
    required: true
  },
  {
    question: "¿Cuál es tu formación? (Marca cuantos sean necesarios)",
    options: [
      "Fisioterapeuta",
      "Masoterapeuta",
      "Enfermero",
      "Acupunturista",
      "Otra"
    ],
    multiple: true,
    slug: "specialities",
    required: true
  },
  {
    question: `Si elegiste "Otra" como tu formación, menciona cúal`,
    slug: "specialities-followup",
    required: true
  },
  {
    question: "¿Tienes experiencia dando masajes?",
    options: ["Sí", "No"],
    slug: "experience",
    required: true
  },
  {
    question: "Menciona años de experiencia dando masajes",
    slug: "yearsExperience",
    required: true
  },
  {
    question: "¿Qué masajes sabes impartir? (marca cuantos sean necesarios)",
    options: [
      "Relajante/Holístico",
      "Linfático/Reductivo",
      "Tejido Profundo/ Descontracturante",
      "Rehabilitación Física",
      "Fisioterapia",
      "Reflexología podal",
      "Acupresión/Shiatsu",
      "Tailandés",
      "Tui Na",
      "Otros"
    ],
    multiple: true,
    slug: "typesKnowledge",
    required: true
  },
  {
    question:
      "¿Cuál es la razón por la que te interesa aprender Masaje Terapéutico Integral CuerpoMente®?",
    slug: "reason",
    required: true
  }
];

const ProspectProfileSchema = new SimpleSchema({
  ...getSchemaQuestions(prospectsQuestions, true)
});

ProspectsSchema = new SimpleSchema(
  {
    firstname: {
      label: "Nombre",
      type: String
    },
    lastname: {
      label: "Apellidos",
      type: String
    },
    email: {
      label: "Correo Electrónico",
      type: String,
      regEx: SimpleSchema.RegEx.Email
    },
    profile: {
      label: "Tu Perfil",
      type: ProspectProfileSchema
    },
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return { $setOnInsert: new Date() };
        } else {
          this.unset(); // Prevent user from supplying their own value
        }
      },
      denyUpdate: true,
      autoform: {
        omit: true
      }
    },
    updatedAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        }
        if (this.isUpdate) {
          return new Date();
        }
      },
      autoform: {
        omit: true
      }
    }
  },
  { tracker: Tracker }
);

Prospects.attachSchema(ProspectsSchema);

export default Prospects;
