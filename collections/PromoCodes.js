import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import times from "lodash.times";
import shuffle from "lodash.shuffle";
import random from "lodash.random";
import SimpleSchema from "simpl-schema";
import { validateUniqueFail } from "./helpers";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

/*
  Generate a random code of 3 capital letters and 3 numbers in random order
 */
export const randomCode = () =>
  shuffle([
    ...times(3, () => String.fromCharCode(random(48, 57))),
    ...times(3, () => String.fromCharCode(random(65, 90)))
  ]).join("");

class PromoCodesCollection extends Mongo.Collection {
  insert(doc, callback) {
    if (doc.code) {
      Meteor.call(
        "validateUnique",
        { collection: "PromoCodes", field: "code", doc },
        err => {
          if (err) {
            validateUniqueFail();
            return false;
          }
          return super.insert(doc, callback);
        }
      );
    } else {
      let promoCode;
      // Get a valid promo code, verifying with db
      do {
        promoCode = randomCode();
      } while (this.findOne({ code: promoCode }));
      doc.code = promoCode;
      return super.insert(doc, callback);
    }
  }
}

PromoCodes = new PromoCodesCollection("promoCodes");

PromoCodes.allow({
  insert: userId => Roles.userIsInRole(userId, ["admin"]),
  update: userId => Roles.userIsInRole(userId, ["admin"]),
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

// This is used by clients to idenitfy it's pending promos
export const PromoCodeInClientSchema = new SimpleSchema({
  type: {
    type: String,
    allowedValues: ["referral"]
  }
});

PromoCodesSchema = new SimpleSchema(
  {
    code: {
      type: String,
      label: "Código de promoción"
    },
    type: {
      type: String,
      label: "Tipo de descuento del código",
      allowedValues: ["amount", "percentage", "newClients", "giftCard"],
      autoform: {
        type: "select-radio-inline",
        options() {
          return [
            {
              value: "amount",
              label: "Restar cantidad al total"
            },
            {
              value: "percentage",
              label: "Porcentaje de descuento"
            }
          ];
        }
      }
    },
    amount: {
      type: Number,
      label:
        "Si es porcentaje, escribir de 0 a 100 sin el signo %. Si es para restar, escribir la cantidad de descuento",
      min: 0,
      defaultValue: 0
    },
    locationsId: {
      label: "Zonas donde estará disponible",
      type: Array,
      defaultValue: [],
      autoform: {
        type: "select-checkbox",
        options() {
          return Locations.find().map(location => ({
            label: location.name,
            value: location._id
          }));
        }
      }
    },
    "locationsId.$": String,
    validCenter: {
      label: "¿El código es válido en el Centro de Bienestar?",
      type: Boolean,
      defaultValue: true
    },
    active: {
      type: Boolean,
      defaultValue: true,
      autoform: {
        omit: true
      }
    },
    isOneTime: {
      label: "¿Es un código de un solo uso?",
      type: Boolean,
      defaultValue: false
    },
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return { $setOnInsert: new Date() };
        } else {
          this.unset(); // Prevent user from supplying their own value
        }
      },
      denyUpdate: true,
      autoform: {
        omit: true
      }
    }
  },
  { tracker: Tracker }
);

PromoCodes.attachSchema(PromoCodesSchema);

export default PromoCodes;
