import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import SimpleSchema from "simpl-schema";

SimpleSchema.extendOptions(["denyUpdate"]);

class OrdersCollection extends Mongo.Collection {
  insert(doc, callback) {
    const client = Clients.findOne(doc.clientId);
    // If order has client, get info from account
    if (client) {
      const { firstname, lastname, email, phone } = client;
      doc = { ...doc, firstname, lastname, email, phone };
    }
    return super.insert(doc, callback);
  }
}

Orders = new OrdersCollection("orders");

Orders.allow({
  insert: () => false,
  update: () => false,
  remove: userId => Roles.userIsInRole(userId, ["admin"])
});

OrdersSchema = new SimpleSchema(
  {
    locationId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true
    },
    items: String,
    total: Number,
    promoCode: {
      type: String,
      optional: true
    },
    clientId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true
    },
    firstname: String,
    lastname: String,
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email
    },
    phone: { type: String, optional: true },
    transactionId: {
      type: String,
      optional: true
    },
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return { $setOnInsert: new Date() };
        } else {
          this.unset(); // Prevent user from supplying their own value
        }
      },
      denyUpdate: true
    }
  },
  { tracker: Tracker }
);

Orders.attachSchema(OrdersSchema);

export default Orders;
