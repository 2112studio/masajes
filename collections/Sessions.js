import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";
import { AddressSchema } from "./helpers";
import { surveyQuestions } from "../common/businessLogic";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

class SessionsCollection extends Mongo.Collection {
  insert(doc, callback) {
    const client = Clients.findOne(doc.clientId);
    // If order has client, get info from account
    if (client && doc.hasAddress) {
      const { address } = client;
      doc = { ...doc, address, locationId: address.locationId };
    }
    return super.insert(doc, callback);
  }
}

Sessions = new SessionsCollection("sessions");

Sessions.allow({
  insert: () => true,
  update: (userId, doc) => Roles.userIsInRole(userId, ["admin"]),
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

export const SurveySchema = new SimpleSchema({
  ...surveyQuestions.reduce(
    (prev, { slug, type, options }) => ({
      ...prev,
      [slug]: {
        type,
        ...(options ? { allowedValues: options.map(i => i.label) } : {})
      }
    }),
    {}
  ),
  practice: {
    type: String,
    optional: true
  }
});

export const SessionsSchema = new SimpleSchema({
  type: {
    type: String,
    autoform: {
      omit: true
    }
  },
  clientId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    denyUpdate: true,
    optional: true,
    autoform: {
      omit: true
    }
  },
  scheduledDate: {
    label: "Fecha",
    type: Date,
    optional: true,
    autoform: {
      omit: true
    }
  },
  checkInTime: {
    label: "Check-in",
    type: Date,
    optional: true,
    autoform: {
      omit: true
    }
  },
  checkInSurvey: {
    type: SurveySchema,
    optional: true,
    autoform: {
      omit: true
    }
  },
  checkOutTime: {
    label: "Check-out",
    type: Date,
    optional: true,
    autoform: {
      omit: true
    }
  },
  checkOutSurvey: {
    type: SurveySchema,
    optional: true,
    autoform: {
      omit: true
    }
  },
  therapistId: {
    label: "Terapeuta",
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
    autoform: {
      firstOption: "(Selecciona)",
      options() {
        return Therapists.find().map(therapist => ({
          label: `${therapist.firstname} ${therapist.lastname}`,
          value: therapist._id
        }));
      }
    }
  },
  orderId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    denyUpdate: true,
    optional: true,
    autoform: {
      omit: true
    }
  },
  locationId: {
    label: "Zona",
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
    autoform: {
      type: "select-radio-inline",
      options() {
        return Locations.find().map(location => ({
          label: location.name,
          value: location._id
        }));
      }
    }
  },
  address: {
    label: "Dirección",
    type: AddressSchema,
    optional: true
  },
  hasAddress: {
    label: "Servicio a domicilio",
    type: Boolean,
    defaultValue: false
  },
  isTreatment: {
    type: Boolean,
    defaultValue: false,
    autoform: {
      omit: true
    }
  },
  status: {
    type: String,
    defaultValue: "unconfirmed",
    allowedValues: [
      "unconfirmed",
      "confirmed",
      "ready",
      "ongoing",
      "canceled",
      "completed"
    ],
    autoform: {
      omit: true
    }
  },
  createdAt: {
    type: Date,
    autoValue: function() {
      if (this.isInsert) {
        return new Date();
      } else if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      } else {
        this.unset(); // Prevent user from supplying their own value
      }
    },
    denyUpdate: true,
    autoform: {
      omit: true
    }
  },
  reminderDay: {
    type: Boolean,
    defaultValue: false,
    autoform: { omit: true }
  },
  reminderTwoDay: {
    type: Boolean,
    defaultValue: false,
    autoform: { omit: true }
  }
});

Sessions.attachSchema(SessionsSchema);

export default Sessions;
