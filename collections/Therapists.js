import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import SimpleSchema from "simpl-schema";
import {
  validateRemoveFail,
  validateRemoveSuccess,
  getSchemaQuestions
} from "./helpers";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

class TherapistsCollection extends Mongo.Collection {
  insert(doc, callback) {
    Meteor.call("createUserFromTherapist", doc, (err, userId) => {
      if (err) {
        console.log(err);
        if (Meteor.isClient) {
          Bert.alert("Todos los campos son requeridos.", "danger");
          $(".careers-submit").prop("disabled", false);
        }
        return false;
      }
      if (Meteor.isClient) {
        Bert.alert("¡Felicidades! Tu registro ha sido existoso", "success");
        FlowRouter.go("careers-success");
      }
      doc.userId = userId;
      return super.insert(doc, callback);
    });
  }

  // Remove therapists that has no orders, and also removes user and promoCode
  remove(selector, callback) {
    Meteor.call("removeTherapists", selector, err => {
      if (err) {
        validateRemoveFail();
        return false;
      }
      this.find(selector).map(item => {
        Meteor.users.remove(item.userId);
      });
      validateRemoveSuccess();
      return super.remove(selector, callback);
    });
  }
}

Therapists = new TherapistsCollection("therapists");

Therapists.allow({
  insert: () => true,
  update: (userId, doc) =>
    Roles.userIsInRole(userId, ["admin"]) || userId === doc.userId,
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

export const denyUpdateFields = ["firstname", "lastname", "email"];

export const therapistsQuestions = [
  {
    question: "Género",
    options: ["Femenino", "Masculino"],
    slug: "gender",
    required: true
  },
  { question: "Fecha de nacimiento", slug: "birthday", required: true },
  { question: "Teléfono de contacto", slug: "phone", required: true },
  {
    question: "¿Cómo te enteraste de esta oportunidad de estudio y trabajo?",
    slug: "findout",
    required: true
  },
  {
    question: "¿Actualmente eres terapeuta corporal?",
    options: ["Sí", "No"],
    slug: "currentTherapist",
    required: true
  },
  {
    question:
      "Universidad/escuela en donde adquiriste o adquieres tus estudios",
    slug: "education",
    required: true
  },
  {
    question: "¿Cuál es tu formación? (Marca cuantos sean necesarios)",
    options: [
      "Fisioterapeuta",
      "Masoterapeuta",
      "Enfermero",
      "Acupunturista",
      "Otra"
    ],
    multiple: true,
    slug: "specialities",
    required: true
  },
  {
    question: `Si elegiste "Otra" como tu formación, menciona cúal`,
    slug: "specialities-followup",
    required: true
  },
  {
    question: "¿Tienes experiencia dando masajes?",
    options: ["Sí", "No"],
    slug: "experience",
    required: true
  },
  {
    question: "Menciona años de experiencia dando masajes",
    slug: "yearsExperience",
    required: true
  },
  {
    question: "¿Qué masajes sabes impartir? (marca cuantos sean necesarios)",
    options: [
      "Relajante/Holístico",
      "Linfático/Reductivo",
      "Tejido Profundo/ Descontracturante",
      "Rehabilitación Física",
      "Fisioterapia",
      "Reflexología podal",
      "Acupresión/Shiatsu",
      "Tailandés",
      "Tui Na",
      "Otros"
    ],
    multiple: true,
    slug: "typesKnowledge",
    required: true
  },
  {
    question:
      "¿Cuál es la razón por la que te interesa aprender Masaje Terapéutico Integral CuerpoMente®?",
    slug: "reason",
    required: true
  },
  {
    question: "¿Te interesa Unirte a nuestro Equipo?",
    options: ["Sí", "No"],
    slug: "joining",
    required: true
  },
  {
    question:
      "¿Tienes auto propio o manera de transportarte por auto para llegar a los clientes?",
    options: ["Sí", "No"],
    slug: "car",
    required: true
  },
  {
    question:
      "¿Tienes Smartphone y datos para ser contactado via whatsapp y correo electrónico?",
    options: ["Sí", "No"],
    slug: "smartphone",
    required: true
  },
  {
    question: "¿Tienes camilla propia?(en buen estado)",
    options: ["Sí", "No"],
    slug: "bed",
    required: true
  },
  {
    question: "Disponibilidad de trabajo (marca los que sean necesarios)",
    options: [
      "Medio",
      "Tiempo Completo",
      "Menos de Medio Tiempo",
      "Estoy abiert@ a trabajar fines de semana"
    ],
    multiple: true,
    slug: "availability",
    required: true
  },
  {
    question: "¿Qué es lo que buscas en un trabajo?",
    slug: "jobFind",
    required: true
  },
  {
    question: "¿Hablas algún otro idioma aparte de español ?(básico)",
    slug: "languages",
    required: true
  },
  {
    question: "Menciona un animal que te guste mucho y porque",
    slug: "animal",
    required: true
  },
  {
    question: "¿Qué te fascina hacer (Hobbies)?",
    slug: "hobbies",
    required: true
  },
  {
    question: "¿En qué zona de la ciudad vives (municipio)?",
    slug: "zone-live",
    required: true
  }
];

const TherapistProfileSchema = new SimpleSchema({
  ...getSchemaQuestions(therapistsQuestions, true)
});

TherapistsSchema = new SimpleSchema(
  {
    firstname: {
      label: "Nombre",
      type: String
    },
    lastname: {
      label: "Apellidos",
      type: String
    },
    email: {
      label: "Correo Electrónico",
      type: String,
      regEx: SimpleSchema.RegEx.Email
    },
    profile: {
      label: "Tu Perfil",
      type: TherapistProfileSchema
    },
    locationsId: {
      label:
        "¿En qué zona de la ciudad estas dispuesto a impartir masajes? (Marca los que sean necesarios)",
      type: Array,
      defaultValue: [],
      autoform: {
        type: "select-checkbox",
        options() {
          return Locations.find().map(location => ({
            label: location.name,
            value: location._id
          }));
        }
      }
    },
    "locationsId.$": String,
    active: {
      type: Boolean,
      defaultValue: true,
      autoform: {
        omit: true
      }
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true,
      autoform: {
        omit: true
      }
    },
    active: {
      type: Boolean,
      defaultValue: false,
      autoform: {
        omit: true
      }
    },
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return { $setOnInsert: new Date() };
        } else {
          this.unset(); // Prevent user from supplying their own value
        }
      },
      denyUpdate: true,
      autoform: {
        omit: true
      }
    },
    updatedAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        }
        if (this.isUpdate) {
          return new Date();
        }
      },
      autoform: {
        omit: true
      }
    }
  },
  { tracker: Tracker }
);

Therapists.attachSchema(TherapistsSchema);

export default Therapists;
