import SimpleSchema from "simpl-schema";
import products from "../common/products";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

export default new SimpleSchema({
  name: String,
  goal: { type: String, optional: true },
  type: {
    type: String,
    allowedValues: products
      .filter(({ isTreatment }) => isTreatment)
      .map(({ slug }) => slug),
    autoform: {
      type: "select-radio-inline",
      options: () =>
        products
          .filter(({ isTreatment }) => isTreatment)
          .map(({ slug, title }) => ({ value: slug, label: title }))
    }
  },
  startDate: {
    type: Date,
    autoform: {
      afFieldInput: {
        type: "datetimepicker"
      }
    }
  },
  endDate: {
    type: Date,
    optional: true,
    autoform: {
      afFieldInput: {
        type: "datetimepicker"
      }
    }
  },
  sessions: {
    type: Array,
    defaultValue: [],
    autoform: {
      omit: true
    }
  },
  "sessions.$": {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    autoform: {
      omit: true
    }
  },
  isFinished: {
    type: Boolean,
    defaultValue: false,
    autoform: { omit: true }
  },
  isCurrent: {
    type: Boolean,
    defaultValue: false,
    autoform: { omit: true }
  },
  amountPaid: {
    type: Number,
    defaultValue: 0,
    autoform: { omit: true }
  },
  discountsApplied: {
    type: Number,
    defaultValue: 0,
    autoform: { omit: true }
  },
  paidInFull: {
    type: Boolean,
    defaultValue: false,
    autoform: { omit: true }
  }
});
