import { Meteor } from "meteor/meteor";
import SimpleSchema from "simpl-schema";
import Locations from "./Locations";

SimpleSchema.extendOptions(["autoform"]);

export const AddressSchema = new SimpleSchema({
  locationId: {
    label: "¿En qué zona te encuentras?",
    type: String,
    optional: true,
    autoform: {
      type: "select-radio-inline",
      options() {
        return Locations.find().map(location => ({
          label: location.name,
          value: location._id
        }));
      }
    }
  },
  street1: {
    label: "Calle y Número",
    type: String,
    optional: true
  },
  street2: {
    label: "Colonia",
    type: String,
    optional: true
  },
  zip: {
    type: String,
    label: "Código postal",
    optional: true
  },
  reference: {
    label: "Referencias del lugar o entre qué calles",
    type: String,
    optional: true
  }
});

export const validateRemoveFail = () => {
  Meteor.isClient &&
    Bert.alert(
      "Hubo un error, revisa las dependencias del elemento",
      "danger",
      "growl-top-right"
    );
};

export const validateUniqueFail = (
  message = "Ya hay un elemento con el mismo nombre"
) => {
  if (Meteor.isClient) Bert.alert(message, "danger", "growl-top-right");
  else throw new Meteor.Error("insert", message);
};

export const validateRemoveSuccess = () => {
  Meteor.isClient &&
    Bert.alert(
      "Éxito! Se ha eliminado ese elemento",
      "success",
      "growl-top-right"
    );
};

export const getSchemaQuestions = (questions, makeRequired) =>
  questions
    .map(({ question, options, multiple, slug, required, type }) => {
      const res = [
        {
          type: multiple ? Array : type || String,
          optional: makeRequired ? !required : true,
          label: question,
          ...(options
            ? {
                autoform: {
                  type: multiple
                    ? "select-checkbox-inline"
                    : "select-radio-inline",
                  options() {
                    return options.map(i => ({ value: i, label: i }));
                  }
                }
              }
            : {})
        }
      ];
      if (multiple) {
        res.push({
          type: String
        });
      }
      return { slug, data: res };
    })
    .reduce((prev, { slug, data }) => {
      return {
        ...prev,
        [`question${slug}`]: data[0],
        ...(data.length > 1 ? { [`question${slug}.$`]: data[1] } : {})
      };
    }, {});
