import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";

SimpleSchema.extendOptions(["denyInsert"]);

Admins = new Mongo.Collection("admins");

Admins.allow({
  insert: () => false,
  update: userId => Roles.userIsInRole(userId, ["superadmin"]),
  remove: () => false
});

AdminsSchema = new SimpleSchema({
  code: {
    type: String
  },
  updatedAt: {
    type: Date,
    autoValue: function() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true
  }
});

Admins.attachSchema(AdminsSchema);

export default Admins;
