import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import SimpleSchema from "simpl-schema";
import {
  validateRemoveFail,
  validateRemoveSuccess,
  validateUniqueFail
} from "./helpers";

SimpleSchema.extendOptions(["autoform", "denyUpdate"]);

class LocationsCollection extends Mongo.Collection {
  insert(doc, callback) {
    Meteor.call(
      "validateUnique",
      { collection: "Locations", field: "name", doc },
      err => {
        if (err) {
          validateUniqueFail();
          return false;
        }
        return super.insert(doc, callback);
      }
    );
  }
  remove(selector, callback) {
    Meteor.call("removeLocations", selector, err => {
      if (err) {
        validateRemoveFail();
        return false;
      }
      validateRemoveSuccess();
      return super.remove(selector, callback);
    });
  }
}

Locations = new LocationsCollection("locations");

Locations.allow({
  insert: userId => Roles.userIsInRole(userId, ["admin"]),
  update: userId => Roles.userIsInRole(userId, ["admin"]),
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

LocationsSchema = new SimpleSchema(
  {
    name: {
      label: "Nombre",
      type: String
    },
    abbreviation: {
      type: String,
      label: "Abreviación"
    },
    active: {
      type: Boolean,
      defaultValue: true,
      autoform: {
        omit: true
      }
    }
  },
  { tracker: Tracker }
);

Locations.attachSchema(LocationsSchema);

export default Locations;
