import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { Tracker } from "meteor/tracker";
import SimpleSchema from "simpl-schema";
import PromoCodes, { PromoCodeInClientSchema } from "./PromoCodes";
import TreatmentsSchema from "./Treatments";
import {
  validateRemoveFail,
  validateRemoveSuccess,
  getSchemaQuestions,
  AddressSchema,
  validateUniqueFail
} from "./helpers";

SimpleSchema.extendOptions(["autoform", "denyInsert", "denyUpdate"]);

class ClientsCollection extends Mongo.Collection {
  insert(doc, callback) {
    try {
      Meteor.call("validateUnique", {
        collection: "Clients",
        field: "email",
        doc
      });
      doc.promoCodeId = PromoCodes.insert({
        type: "newClients"
      });
      return super.insert(doc, callback);
    } catch (error) {
      Meteor.call("removeUserFromClient", doc.userId);
      validateUniqueFail("El email del cliente ya existe");
      return false;
    }
  }

  update(selector, modifier, options, callback) {
    const { $set, $unset } = modifier;
    if ($set) {
      // Check if any required address fields is unset
      if (
        $unset &&
        ["street1", "street2", "zip", "locationId"].reduce(
          (prev, i) => prev || $unset[`address.${i}`] === "",
          false
        )
      ) {
        $set.hasAddress = false;
      } else {
        $set.hasAddress = true;
      }
      // Call original function
      modifier.$set = $set;
    }
    return super.update(selector, modifier, options, callback);
  }

  // Remove clients that has no orders, and also removes user and promoCode
  remove(selector, callback) {
    Meteor.call("removeClients", selector, err => {
      if (err) {
        validateRemoveFail();
        return false;
      }
      validateRemoveSuccess();
      return super.remove(selector, callback);
    });
  }

  // Function to remove client without removing user. This is used to promote client to admin
  partialRemove(selector, callback) {
    Meteor.call("partialRemoveClients", selector, err => {
      if (err) {
        validateRemoveFail();
        return false;
      }
      validateRemoveSuccess();
      return super.remove(selector, callback);
    });
  }
}

Clients = new ClientsCollection("clients");

Clients.allow({
  insert: () => true,
  update: (userId, doc) =>
    Roles.userIsInRole(userId, ["admin"]) || userId === doc.userId,
  remove: userId => Roles.userIsInRole(userId, ["superadmin"])
});

export const clientQuestions = [
  {
    question: "Fecha de nacimiento",
    required: true,
    slug: "birthday",
    type: Date
  },
  {
    question: "¿Cómo supiste de nosotros?",
    options: [
      "Recomendación",
      "Facebook",
      "Google",
      "Anuncio Exterior",
      "Evento"
    ],
    required: true,
    slug: "findout"
  },
  {
    question:
      "Si alguien te recomendó, ¿Cuál es el nombre completo de esa persona?",
    required: true,
    slug: "recommendation"
  },
  {
    question:
      "¿Qué te llamó la atención de lo que viste/escuchaste/sentiste/entendiste al saber de nosotros?",
    required: true,
    slug: "impression"
  },
  {
    question: "¿Estas bajo tratamiento médico?",
    options: ["Sí", "No"],
    required: true,
    slug: "medicalTreatment"
  },
  {
    question: "De ser así, ¿De qué tipo?",
    required: true,
    slug: "type"
  },
  {
    question: "¿Tienes alguna enfermedad u operación reciente?",
    options: ["Sí", "No"],
    required: true,
    slug: "sickness"
  },
  {
    question: "De ser así, ¿De qué tipo?",
    required: true,
    slug: "sicknessType"
  },
  {
    question:
      "¿Por qué estas aquí? ¿Cuál es tu motivación inicial al estar aquí?",
    required: true,
    slug: "reason"
  },
  {
    question:
      "¿Cuánto de tu tiempo estás dispuesto/a a dedicarle a tus Prácticas diarias para la incorporación de Hábitos y lograr tus objetivos?",
    options: ["5 min", "15-25 min", "No estoy dispuesto/a a dedicarle tiempo"],
    required: true,
    slug: "time"
  },
  {
    question: "¿Qué te fascina hacer (hobbies)?",
    required: true,
    slug: "hobbies"
  }
];

const ClientProfileSchema = new SimpleSchema({
  ...getSchemaQuestions(clientQuestions)
});

const ClientsSchema = new SimpleSchema(
  {
    firstname: {
      type: String,
      autoform: {
        omit: true
      }
    },
    lastname: {
      type: String,
      autoform: {
        omit: true
      }
    },
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email,
      autoform: {
        omit: true
      }
    },
    phone: {
      label: "Teléfono",
      type: String,
      optional: true
    },
    address: {
      label: "Dirección",
      type: AddressSchema,
      optional: true
    },
    profile: {
      label: "Tu Perfil",
      optional: true,
      type: ClientProfileSchema
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true,
      autoform: {
        omit: true
      }
    },
    treatments: {
      type: Array,
      defaultValue: [],
      autoform: {
        omit: true
      }
    },
    "treatments.$": {
      type: TreatmentsSchema
    },
    promoCodeId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true,
      optional: true,
      autoform: {
        omit: true
      }
    },
    giftCardAmount: {
      type: Number,
      defaultValue: 0,
      autoform: {
        omit: true
      }
    },
    createdAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        } else if (this.isUpsert) {
          return { $setOnInsert: new Date() };
        } else {
          this.unset(); // Prevent user from supplying their own value
        }
      },
      denyUpdate: true,
      autoform: {
        omit: true
      }
    },
    updatedAt: {
      type: Date,
      autoValue: function() {
        if (this.isInsert) {
          return new Date();
        }
        if (this.isUpdate) {
          return new Date();
        }
      },
      autoform: {
        omit: true
      }
    },
    pendingPromos: {
      type: Array,
      defaultValue: [],
      autoform: {
        omit: true
      }
    },
    "pendingPromos.$": PromoCodeInClientSchema,
    isNewUser: {
      type: Boolean,
      defaultValue: true,
      autoform: {
        omit: true
      }
    },
    hasAddress: {
      type: Boolean,
      defaultValue: false,
      autoform: { omit: true }
    },
    conektaId: {
      type: String,
      optional: true,
      autoform: { omit: true }
    },
    last4: { type: String, optional: true, autoform: { omit: true } }
  },
  { tracker: Tracker }
);

Clients.attachSchema(ClientsSchema);

export default Clients;
