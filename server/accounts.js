AccountsTemplates.configure({
  postSignUpHook: (userId, info) => {
    Meteor.call("createClientFromSignUp", userId, info, true);
  }
});

Accounts.emailTemplates.resetPassword = {
  from: () => `[Abril Bienestar Integral] <${process.env.SENDER_EMAIL}>`,
  subject: () => "[Abril Bienestar Integral] Reestablecer contraseña",
  text: (_, url) => `
      Hola:

      Hemos recibido una solicitud para reestablecer tu contraseña. Solo tienes que entrar al siguiente link ${url}.

      Si esta es una solicitud erronea, por favor ignorala.

      Abril Bienestar Integral
    `
};

Accounts.emailTemplates.enrollAccount = {
  from: () => `[Abril Bienestar Integral] <${process.env.SENDER_EMAIL}>`,
  subject: () =>
    "[Abril Bienestar Integral] Gracias por querer unirte al equipo!",
  text: (_, url) => `
      Hola:

      Gracias por mostrar interés en unirte a nuestro equipo y así llegar a más personas que buscan crear una feliz vida en Bienestar Integral, hemos recibido tu solicitud  y en breve te daremos una respuesta.

      Para esto, se te creó una cuenta con el email de la solicitud. Solo debes entrar al siguiente link y crear la contraseña de tu cuenta.
      ${url}

      Si esta es una solicitud errónea, por favor ignorala.

      Abril Bienestar Integral
    `
};
