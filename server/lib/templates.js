export default {
  welcome: {
    path: "emailTemplates/welcome.html", // Relative to the 'private' dir.
    route: {
      path: "/welcome",
      data: () => ({ firstname: "Alex" })
    }
  },
  newOrderClient: {
    path: "emailTemplates/newOrderClient.html",
    route: { path: "/newOrderClient" } // Just to silence warning
  },
  newOrderAdmin: {
    path: "emailTemplates/newOrderAdmin.html",
    route: { path: "/newOrderAdmin" } // Just to silence warning
  },
  reminderDay: {
    path: "emailTemplates/reminderDay.html",
    route: { path: "/reminderDay" }
  },
  reminderTwoDay: {
    path: "emailTemplates/reminderTwoDay.html",
    route: { path: "/reminderTwoDay" }
  },
  reschedule: {
    path: "emailTemplates/reschedule.html",
    route: { path: "/reschedule" }
  },
  business: {
    path: "emailTemplates/business.html",
    route: { path: "/business" }
  },
  therapistAdmission: {
    path: "emailTemplates/therapistAdmission.html",
    route: { path: "/therapistAdmission" }
  },
  newTherapist: {
    path: "emailTemplates/newTherapist.html",
    route: { path: "/newTherapist" }
  },
  nextSession: {
    path: "emailTemplates/nextSession.html",
    route: { path: "/nextSession" }
  },
  giftCard: {
    path: "emailTemplates/giftCard.html",
    route: {
      path: "/giftCard",
      data: () => ({ amount: "$100.00", code: "1qaxsw2", sender: "Anónimo" })
    }
  }
};
