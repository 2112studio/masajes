export default {
  prettyPrice: (amount = 0) =>
    amount.toLocaleString("en-US", { style: "currency", currency: "USD" }),
  humanDatetime: date => moment(date).format("llll"),
  adminEmail: () => "admin@abrilbienestarintegral.com"
};
