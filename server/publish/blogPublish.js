import { Meteor } from "meteor/meteor";
import Posts from "../../collections/Posts";

Meteor.publish("posts", () => Posts.find());

Meteor.publish("post", _id => Posts.find({ _id }));

Meteor.publish("postBySlug", slug => Posts.find({ slug }));

Meteor.publish("postsLimit", limit => Posts.sorted({ limit }));

Meteor.publish("popularPosts", limit =>
  Posts.sorted({ sortBy: "views", limit, desc: true })
);
