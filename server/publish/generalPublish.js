import { Meteor } from "meteor/meteor";
import Admins from "../../collections/Admins";
import Locations from "../../collections/Locations";

// ADMINS
Meteor.publish("admins", () =>
  Meteor.users.find({ roles: { $nin: ["client"] } })
);
Meteor.publish("adminCode", () => Admins.find());

// LOCATIONS
Meteor.publish("locations", () => Locations.find({ active: true }));
Meteor.publish("allLocations", () => Locations.find());
