import { Meteor } from "meteor/meteor";
import Prospects from "../../collections/Prospects";

Meteor.publish("prospects", () => Prospects.find({}));
