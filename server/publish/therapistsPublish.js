import { Meteor } from "meteor/meteor";
import Therapists from "../../collections/Therapists";
import { pendingStatus, pastStatus } from "../../common/businessLogic";

Meteor.publish("currentTherapist", function() {
  return Therapists.find({ userId: this.userId });
});

Meteor.publish("therapistsRequests", () => Therapists.find({ active: false }));

Meteor.publish("currentTherapists", () => Therapists.find({ active: true }));

Meteor.publish("pendingSessionsTherapists", () => {
  const therapists = Sessions.find({ status: { $in: pendingStatus } }).map(
    item => item.therapistId
  );
  return Therapists.find({ _id: { $in: therapists } });
});

Meteor.publish("pastSessionsTherapists", () => {
  const therapists = Sessions.find({
    status: { $in: pastStatus }
  }).map(item => item.therapistId);
  return Therapists.find({ _id: { $in: therapists } });
});

Meteor.publish("clientSessionsTherapists", userId => {
  if (!userId) return [];
  const currClient = Clients.findOne({ userId }) || {};
  const therapistsId = Sessions.find({
    clientId: currClient._id
  }).map(({ therapistId }) => therapistId);
  return Therapists.find({ _id: { $in: therapistsId } });
});
