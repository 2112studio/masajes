import { Meteor } from "meteor/meteor";
import PromoCodes from "../../collections/PromoCodes";
import Clients from "../../collections/Clients";

Meteor.publish("clientPromoCode", function() {
  const { promoCodeId } = Clients.findOne({ userId: this.userId }) || {};
  return PromoCodes.find(promoCodeId);
});

Meteor.publish("regularPromoCodes", locationId =>
  PromoCodes.find({
    type: { $ne: "newClients" },
    ...(locationId ? { locationsId: locationId } : {})
  })
);
