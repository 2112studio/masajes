import { Meteor } from "meteor/meteor";
import Clients from "../../collections/Clients";
import Sessions from "../../collections/Sessions";
import { pendingStatus, pastStatus } from "../../common/businessLogic";

Meteor.publish("currentClient", function() {
  return Clients.find({ userId: this.userId });
});

Meteor.publish("clientById", userId => Clients.find({ userId }));

Meteor.publish("clients", () => Clients.find());

Meteor.publish("pendingSessionsClients", () => {
  const clients = Sessions.find({ status: { $in: pendingStatus } }).map(
    item => item.clientId
  );
  return Clients.find({ _id: { $in: clients } });
});

Meteor.publish("therapistPendingSessionsClients", function() {
  if (!this.userId) return [];
  const currTherapist = Therapists.findOne({ userId: this.userId }) || {};
  const clients = Sessions.find({
    status: { $in: pendingStatus },
    therapistId: currTherapist._id
  }).map(item => item.clientId);
  return Clients.find({ _id: { $in: clients } });
});

Meteor.publish("pastSessionsClients", () => {
  const clients = Sessions.find({
    status: { $in: pastStatus }
  }).map(item => item.clientId);
  return Clients.find({ _id: { $in: clients } });
});

Meteor.publish("therapistPastSessionsClients", function() {
  if (!this.userId) return [];
  const currTherapist = Therapists.findOne({ userId: this.userId }) || {};
  const clients = Sessions.find({
    status: { $in: pastStatus },
    therapistId: currTherapist._id
  }).map(item => item.clientId);
  return Clients.find({ _id: { $in: clients } });
});

Meteor.publish("clientPendingSessions", function() {
  if (this.userId) {
    const clientId = Clients.findOne({ userId: this.userId })._id;
    return Sessions.find({ clientId, status: { $in: pendingStatus } });
  }
  return [];
});

Meteor.publish("clientsWithPendingSessions", () => {
  const sessions = Sessions.find({ status: { $in: pendingStatus } }).map(
    ({ clientId }) => clientId
  );
  return Clients.find({ _id: { $in: sessions } });
});

Meteor.publish("ongoingSessionClient", () => {
  const session =
    Sessions.findOne({ status: "ongoing" }, { sort: { scheduledDate: 1 } }) ||
    {};
  return Clients.find({ _id: session.clientId });
});
