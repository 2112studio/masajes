import { Meteor } from "meteor/meteor";
import Sessions from "../../collections/Sessions";
import Therapists from "../../collections/Therapists";
import { pendingStatus, pastStatus } from "../../common/businessLogic";

Meteor.publish("pendingSessions", () =>
  Sessions.find({ status: { $in: pendingStatus } })
);

Meteor.publish("therapistPendingSessions", function() {
  const userId = this.userId;
  if (!userId) return [];
  const currTherapist = Therapists.findOne({ userId }) || {};
  return Sessions.find({
    status: { $in: pendingStatus },
    therapistId: currTherapist._id
  });
});

Meteor.publish("pendingUnassignedSessions", () =>
  Sessions.find({
    status: { $in: pendingStatus },
    therapistId: { $in: [null, ""] }
  })
);

Meteor.publish("pastSessions", () =>
  Sessions.find({ status: { $in: pastStatus } })
);

Meteor.publish("therapistPastSessions", function() {
  const userId = this.userId;
  if (!userId) return [];
  const currTherapist = Therapists.findOne({ userId }) || {};
  return Sessions.find({
    status: { $in: pastStatus },
    therapistId: currTherapist._id
  });
});

Meteor.publish("therapistOngoingSessions", function() {
  const userId = this.userId;
  if (!userId) return [];
  const currTherapist = Therapists.findOne({ userId }) || {};
  return Sessions.find({
    status: { $in: ["ongoing", "ready"] },
    therapistId: currTherapist._id
  });
});

Meteor.publish("clientOngoingSessions", function() {
  const userId = this.userId;
  if (!userId) return [];
  const currClient = Clients.findOne({ userId }) || {};
  return Sessions.find({
    status: { $in: ["ongoing"] },
    clientId: currClient._id
  });
});

Meteor.publish("clientSessions", userId => {
  if (!userId) return [];
  const currClient = Clients.findOne({ userId }) || {};
  return Sessions.find({
    clientId: currClient._id
  });
});

Meteor.publish("clientCurrentTreatmentSessions", () => {
  const { treatments = [] } =
    Clients.findOne({ userId: Meteor.userId() }) || {};
  const { sessions = [] } = treatments.find(({ isCurrent }) => isCurrent) || {};
  return Sessions.find({
    _id: { $in: sessions }
  });
});
