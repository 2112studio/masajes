import { Meteor } from "meteor/meteor";
import Clients from "../../collections/Clients";
import PromoCodes from "../../collections/PromoCodes";
import products from "../../common/products";

Meteor.methods({
  toggleActivePromoCode: _id => {
    let active = PromoCodes.findOne(_id).active;
    PromoCodes.update(_id, { $set: { active: !active } });
  },
  validatePromoCode: ({ code, isNewUser, locationId }) => {
    const promoCode = PromoCodes.findOne({ code });
    if (!promoCode) {
      throw new Meteor.Error(
        "invalid-promo-code",
        "El código de promoción no existe"
      );
    }
    if (!promoCode.active) {
      throw new Meteor.Error(
        "invalid-promo-code",
        "El código de promoción ya no está disponible"
      );
    }
    if (promoCode.type === "newClients") {
      if (!isNewUser) {
        const client = Clients.findOne({ userId: Meteor.userId() });
        if (!client) {
          throw new Meteor.Error(
            "invalid-promo-code",
            "Los datos son invalidos"
          );
        }
        if (!client.isNewUser) {
          throw new Meteor.Error(
            "invalid-promo-code",
            "Este código solo es para nuevos usuarios"
          );
        }
        if (client.promoCodeId === promoCode._id) {
          throw new Meteor.Error(
            "invalid-promo-code",
            "El código es solo para invitar a nuevos usuarios"
          );
        }
      }
    } else if (
      locationId &&
      !promoCode.locationsId.find(i => i === locationId)
    ) {
      throw new Meteor.Error(
        "invalid-promo-code",
        "El código de promoción no está disponible en esa zona"
      );
    } else if (!locationId && !promoCode.validCenter) {
      throw new Meteor.Error(
        "invalid-promo-code",
        "El código de promoción no está disponible en el Centro de Bienestar"
      );
    } else if (promoCode.type === "giftCard") {
      throw new Meteor.Error(
        "invalid-promo-code",
        "Tu certificado de regalo debe ser registrado en tu cuenta"
      );
    }
    return promoCode;
  },
  redeemPromocode: (promoCodeId, type) => {
    const product = products.find(({ slug }) => slug === type);
    const promoCode = PromoCodes.findOne(promoCodeId);
    if (!product || !promoCode) return false;

    // Remove one time promocodes
    if (promoCode.isOneTime) {
      PromoCodes.remove({ _id: promoCodeId._id });
    }

    switch (promoCode.type) {
      case "newClients":
        const promoCodeOwner = Clients.findOne({ promoCodeId: promoCode._id });
        Clients.update(promoCodeOwner._id, {
          $push: { pendingPromos: { type: "referral" } }
        });
    }
  }
});
