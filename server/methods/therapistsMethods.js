import { Meteor } from "meteor/meteor";
import Sessions from "../../collections/Sessions";
import Therapists from "../../collections/Therapists";

Meteor.methods({
  createUserFromTherapist: ({ email, firstname, lastname }) => {
    if (!email || !firstname || !lastname) {
      throw new Meteor.Error("has-dependency", `Incomplete data`);
    }
    const userId = Accounts.createUser({
      email,
      profile: {
        firstname,
        lastname
      }
    });
    Accounts.sendEnrollmentEmail(userId);
    Roles.addUsersToRoles(userId, "unassigned");
    Mailer.send({
      to: process.env.ADMIN_EMAIL,
      subject: "[Abril Bienestar Integral] Nueva solicitud de terapeuta!",
      template: "newTherapist"
    });
    return userId;
  },
  removeTherapists: selector => {
    const ids = Therapists.find(selector).map(item => item._id);
    if (Sessions.find({ therapistId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "has-dependency",
        `Item can't be deleted because it has dependency`
      );
    }
  },
  toggleActiveTherapist: _id => {
    let { active, userId, email, firstname } = Therapists.findOne(_id);
    if (active) {
      Roles.removeUsersFromRoles(userId, "therapist");
      Roles.addUsersToRoles(userId, "unassigned");
    } else {
      Roles.removeUsersFromRoles(userId, "unassigned");
      Roles.addUsersToRoles(userId, "therapist");
      Mailer.send({
        to: email,
        subject: "[Abril Bienestar Integral] Felicidades, has sido admitido",
        template: "therapistAdmission",
        data: { firstname }
      });
    }
    Therapists.update(_id, { $set: { active: !active } });
  }
});
