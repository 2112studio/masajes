import "./clientsMethods";
import "./promoCodesMethods";
import "./locationsMethods";
import "./generalMethods";
import "./therapistsMethods";
import "./purchaseMethods";
import "./sessionMethods";
import "./blogMethods";
