import { Meteor } from "meteor/meteor";
import times from "lodash.times";
import Sessions from "../../collections/Sessions";
import products from "../../common/products";
import Orders from "../../collections/Orders";

Meteor.methods({
  assignSessionTherapist: (sessionId, therapistId) => {
    Sessions.update({ _id: sessionId }, { $set: { therapistId } });
  },
  startSession: () => {
    // This is always performed by therapist
    const therapist = Therapists.findOne({ userId: Meteor.userId() });
    const { _id } = Sessions.findOne(
      {
        status: { $in: ["ready", "ongoing"] },
        therapistId: therapist._id
      },
      { sort: { scheduledDate: 1 } }
    );
    Sessions.update(
      { _id },
      { $set: { status: "ongoing", checkInTime: moment().toISOString() } }
    );
  },
  finishSession: survey => {
    Meteor.call("addSurvey", survey, false);
    // This is always performed by client
    const client = Clients.findOne({ userId: Meteor.userId() });
    const session = Sessions.findOne(
      {
        status: "ongoing",
        clientId: client._id
      },
      { sort: { scheduledDate: 1 } }
    );
    if (!session) return;

    // Check if we need to finish treatment
    if (session.isTreatment) {
      const treatmentIndex = client.treatments.findIndex(
        ({ isCurrent }) => isCurrent
      );
      const treatment = client.treatments[treatmentIndex];
      const sessionIndex = treatment.sessions.findIndex(i => i === session._id);
      if (sessionIndex === treatment.sessions.length - 1) {
        const newTreatments = client.treatments;
        newTreatments[treatmentIndex] = {
          ...client.treatments[treatmentIndex],
          isFinished: true,
          isCurrent: false,
          endDate: moment().toISOString()
        };
        Clients.update(
          { _id: client._id },
          { $set: { treatments: newTreatments } }
        );
      }
    }

    Sessions.update(
      { _id: session._id },
      { $set: { status: "completed", checkOutTime: moment().toISOString() } }
    );
  },
  treatmentFromSession: sessionId => {
    const session = Sessions.findOne({ _id: sessionId });
    if (!session) {
      throw new Meteor.Error("generic-error", "La sesión no existe");
    }
    const client = Clients.findOne({ _id: session.clientId });
    if (!client) {
      throw new Meteor.Error("generic-error", "El cliente no existe");
    }
    const order = Orders.findOne({ _id: session.orderId });
    if (!order.total) {
      throw new Meteor.Error(
        "generic-error",
        "No se puede convertir esta sesión a Circuito"
      );
    }
    const currTreatment = client.treatments.find(({ isCurrent }) => isCurrent);
    if (currTreatment) {
      throw new Meteor.Error("generic-error", "El cliente está en un Circuito");
    }
    const product = products.find(({ isTreatment }) => isTreatment);
    // Insert remanining sessions unconfirmed
    const restSessions = times(product.numberOfSessions - 1, () =>
      Sessions.insert({
        type: product.slug,
        clientId: client._id,
        hasAddress: session.hasAddress,
        isTreatment: true,
        status: "unconfirmed"
      })
    );
    // Update client treatments
    Clients.update(
      { _id: client._id },
      {
        $push: {
          treatments: {
            name: product.title,
            type: product.slug,
            startDate: session.scheduledDate,
            sessions: [sessionId, ...restSessions],
            isCurrent: true,
            amountPaid: order.total
          }
        }
      }
    );
    Sessions.update({ _id: sessionId }, { $set: { isTreatment: true } });
  },
  rescheduleSession: (_id, scheduledDate) => {
    if (Roles.userIsInRole(Meteor.userId(), "admin")) {
      Sessions.update(
        { _id },
        { $set: { scheduledDate, status: "confirmed" } }
      );
    } else {
      const session = Sessions.findOne(_id);
      if (!session) {
        throw new Meteor.Error("invalid-session", "La sesión es incorrecta");
      }
      const client = Clients.findOne(session.clientId);
      if (!client) {
        throw new Meteor.Error("invalid-session", "La sesión es incorrecta");
      }
      Mailer.send({
        to: process.env.ADMIN_EMAIL,
        subject:
          "[Abril Bienestar Integral] Un cliente ha solicitado reagendar su sesión",
        template: "reschedule",
        data: {
          name: `${client.firstname} ${client.lastname}`,
          email: client.email,
          phone: client.phone,
          originalDate: session.scheduledDate,
          scheduleDate: scheduledDate
        }
      });
    }
  },
  addSurvey: (survey, start) => {
    const { _id } = Sessions.findOne(
      { status: { $in: ["ongoing"] } },
      { sort: { scheduledDate: 1 } }
    );
    if (start) {
      Sessions.update({ _id }, { $set: { checkInSurvey: survey } });
    } else {
      Sessions.update({ _id }, { $set: { checkOutSurvey: survey } });
    }
  }
});
