import { Meteor } from "meteor/meteor";
import Posts from "../../collections/Posts";

Meteor.methods({
  createPost: post => Posts.insert(post),
  updatePost: (_id, post) => Posts.update(_id, { $set: post }),
  addVisitPost: slug => {
    let { views } = Posts.findOne({ slug }) || {};
    Posts.update({ slug }, { $set: { views: views + 1 } });
  }
});
