import { Meteor } from "meteor/meteor";
import { Roles } from "meteor/alanning:roles";
import Clients from "../../collections/Clients";
import Sessions from "../../collections/Sessions";
import PromoCodes from "../../collections/PromoCodes";
import products from "../../common/products";

Meteor.methods({
  createClientFromSignUp: (userId, info, sendEmail) => {
    const { email } = info;
    const { firstname, lastname } = info.profile;
    Roles.setUserRoles(userId, "client");
    if (sendEmail) {
      Mailer.send({
        to: email,
        subject: `[Abril Bienestar Integral] Bienvenido!`,
        template: "welcome",
        data: { firstname }
      });
    }
    return Clients.insert({ email, firstname, lastname, userId });
  },
  createClient: (
    { firstname, lastname, email, password, repeatPassword },
    address,
    needsAddress
  ) => {
    if (
      !firstname ||
      !lastname ||
      !email ||
      !password ||
      !repeatPassword ||
      (needsAddress && !address)
    ) {
      throw new Meteor.Error(
        "user-error",
        `Los datos de la cuenta están incorrectos, intenta de nuevo`
      );
    }

    const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegEx.test(email)) {
      throw new Meteor.Error("user-error", `El email es invalido`);
    }

    if (password !== repeatPassword) {
      throw new Meteor.Error("user-error", `Las dos contraseñas no coinciden`);
    }

    const userId = Accounts.createUser({
      email,
      password,
      profile: { firstname, lastname }
    });
    const clientId = Meteor.call(
      "createClientFromSignUp",
      userId,
      {
        email,
        profile: { firstname, lastname }
      },
      false
    );
    if (!clientId) {
      throw new Meteor.Error(
        "invalid-user",
        "Hubo un error en tu orden. Intenta de nuevo"
      );
    }
    if (needsAddress) {
      Clients.update(clientId, { $set: { address } });
    }

    return userId;
  },
  partialRemoveClients: selector => {
    const ids = Clients.find(selector).map(item => item._id);
    if (Sessions.find({ clientId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "client-has-orders",
        `Client can't be deleted because it has sessions`
      );
    }

    Clients.find(selector).map(item => {
      PromoCodes.remove(item.promoCodeId);
    });
  },
  removeClients: selector => {
    const ids = Clients.find(selector).map(item => item._id);
    if (Sessions.find({ clientId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "client-has-orders",
        `Client can't be deleted because it has orders`
      );
    }
    Clients.find(selector).map(item => {
      Meteor.users.remove(item.userId);
      PromoCodes.remove(item.promoCodeId);
    });
  },
  removeUserFromClient: userId => {
    Meteor.users.remove(userId);
  },
  setTreatmentGoal: (treatmentIndex, goal) => {
    const { _id, treatments } = Clients.findOne({ userId: Meteor.userId() });
    treatments[treatmentIndex].goal = goal;
    Clients.update({ _id }, { $set: { treatments } });
  },
  addCashTreatment: (treatmentIndex, clientId, amount) => {
    if (!Roles.userIsInRole(Meteor.userId(), "superadmin")) {
      throw new Meteor.Error(
        "invalid-user",
        `No está autorizado para realizar esta operación`
      );
    }
    const parsedAmount = Number(amount);
    if (!parsedAmount) {
      throw new Meteor.Error("invalid-amount", `El monto es incorrecto`);
    }
    const { treatments } = Clients.findOne(clientId);
    // Add cash to treatment amount paid
    treatments[treatmentIndex].amountPaid += parsedAmount;
    // We need to verify if this is the last payment so all the discounts are applied
    const { amountPaid, sessions, type } = treatments[treatmentIndex];
    // Get current session of treatment
    const currentSessionNumber =
      sessions.findIndex(i => Sessions.findOne(i).status === "confirmed") + 1;
    const { partialPaymentsSessions } = products.find(
      ({ slug }) => slug === type
    );
    const lastSessionPayment =
      partialPaymentsSessions[partialPaymentsSessions.length - 1];
    // If last session, check if client had referrals to apply discount
    if (currentSessionNumber === lastSessionPayment) {
      const finalPrice = Meteor.call("getFinalReferralPrice", type, clientId);
      // If amount is greater or equal than discounted price, set treatment as paid
      if (amountPaid + parsedAmount >= finalPrice) {
        treatments[treatmentIndex].paidInFull = true;
      }
    }
    // Update treatments
    Clients.update(clientId, { $set: { treatments } });
  },
  getFinalReferralPrice: (type, clientId) => {
    const client = Clients.findOne(clientId);
    if (!client) {
      throw new Meteor.Error("invalid-user", `El cliente no existe`);
    }
    const product = products.find(({ slug }) => slug === type);
    if (!product) {
      throw new Meteor.Error("invalid-user", `El producto no existe`);
    }
    const { price, referralDiscounts } = product;
    const { pendingPromos } = client;
    // If no referrals, just return price
    if (!pendingPromos.length) return price;
    // Count the number of referrals
    const numberOfReferrals = pendingPromos.reduce(
      (prev, { type }) => (type && type === "referral" ? prev + 1 : prev),
      0
    );
    // Normalize index and subtract 1 to target correct element with index
    const referralIndex =
      (numberOfReferrals > referralDiscounts.length
        ? referralDiscounts.length
        : numberOfReferrals) - 1;
    return price - referralDiscounts[referralIndex];
  },
  cancelTreatment: (treatmentIndex, clientId) => {
    const { treatments } = Clients.findOne(clientId);
    treatments[treatmentIndex].sessions.map(_id => {
      const session = Sessions.findOne({ _id });
      if (session && session.status === "confirmed") {
        Sessions.update({ _id }, { $set: { status: "canceled" } });
      }
    });
    treatments[treatmentIndex].isFinished = true;
    treatments[treatmentIndex].isCurrent = false;
    treatments[treatmentIndex].endDate = moment().toISOString();
    Clients.update(clientId, { $set: { treatments } });
  }
});
