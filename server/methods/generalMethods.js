import { Meteor } from "meteor/meteor";
import conekta from "conekta";
import Admins from "../../collections/Admins";
import Clients from "../../collections/Clients";
import PromoCodes, { randomCode } from "../../collections/PromoCodes";
import Locations from "../../collections/Locations";

Meteor.methods({
  // ADMIN
  refreshAdminCode: () => {
    const { _id } = Admins.findOne();
    Admins.update({ _id: _id }, { $set: { code: randomCode() } });
  },
  // Having wrapper methods for front-end convenience
  toggleAdminRole: id => Meteor.call("toggleRole", id, "admin"),
  toggleEditorRole: id => Meteor.call("toggleRole", id, "editor"),
  toggleRole: (id, role) => {
    if (Roles.userIsInRole(id, "superadmin")) return;
    const isActive = Roles.userIsInRole(id, role);
    if (isActive) {
      // When smalles permission is removed, remove all permissions and move to a unassigned role
      if (role === "editor") {
        Roles.addUsersToRoles(id, "unassigned");
        Roles.removeUsersFromRoles(id, ["admin", "editor"]);
      } else Roles.removeUsersFromRoles(id, role);
    } else {
      if (role === "admin") Roles.addUsersToRoles(id, ["admin", "editor"]);
      else Roles.addUsersToRoles(id, role);
      Roles.removeUsersFromRoles(id, "unassigned");
    }
  },
  requestAdmin: inputCode => {
    const { code } = Admins.findOne();
    const userId = Meteor.userId();
    if (inputCode === code) {
      // Verify if client has no orders, and remove promoCodes
      Clients.partialRemove({ userId });
      Roles.removeUsersFromRoles(userId, "client");
      Roles.addUsersToRoles(userId, "unassigned");
    } else {
      throw new Meteor.Error("code-invalid");
    }
  },
  validateUnique: ({ collection: collectionName, field, doc }) => {
    // Can't pass colletion directly
    const collection = [
      { name: "PromoCodes", value: PromoCodes },
      { name: "Clients", value: Clients },
      { name: "Locations", value: Locations }
    ].find(({ name }) => name === collectionName).value;
    const sameObject = collection.findOne({ [field]: doc[field] });
    if (sameObject) {
      throw new Meteor.Error("not-unique", "Ya hay un objeto con este nombre");
    }
  },
  sendBienestarCorporativo: data =>
    Mailer.send({
      to: process.env.ADMIN_EMAIL,
      subject:
        "[Abril Bienestar Integral] Nueva solicitud de Bienestar Corporativo",
      template: "business",
      data: { data }
    }),
  getConektaKey: () => process.env.CONEKTA_PUBLIC,
  purchaseGiftCard: ({
    email,
    name,
    amount,
    cardholder,
    token,
    cardHolderEmail
  }) => {
    if (!email || !amount || !cardholder || !token || !cardHolderEmail) {
      throw new Meteor.Error(
        "invalid-purchase",
        "Los datos son incorrectos. Intenta de nuevo"
      );
    }
    let promoCodeId;
    let prettyAmount = Number(amount);
    if (prettyAmount === NaN) {
      throw new Meteor.Error(
        "invalid-purchase",
        "La cantidad es invalida. Intenta de nuevo"
      );
    }
    prettyAmount = amount.toLocaleString("en-US", {
      style: "currency",
      currency: "USD"
    });

    try {
      promoCodeId = PromoCodes.insert({
        type: "giftCard",
        amount,
        isOneTime: true
      });
      const { code } = PromoCodes.findOne(promoCodeId);
      // Add conekta client
      const customer = Meteor.wrapAsync(
        conekta.Customer.create,
        conekta.Customer
      )({
        name: cardholder,
        email: cardHolderEmail,
        payment_sources: [
          {
            type: "card",
            token_id: token
          }
        ]
      });
      const { id: conektaId } = customer.toObject();

      const order = Meteor.wrapAsync(conekta.Order.create, conekta.Order)({
        line_items: [
          {
            name: `Tarjeta de Regalo: ${prettyAmount}`,
            unit_price: amount * 100,
            quantity: 1
          }
        ],
        currency: "MXN",
        customer_info: {
          customer_id: conektaId
        },
        charges: [
          {
            payment_method: {
              type: "default"
            }
          }
        ]
      });
      const { id } = order.toObject();
      const [firstname, lastname] = cardholder.split(" ");
      Orders.insert({
        firstname,
        lastname,
        email: cardHolderEmail,
        transactionId: id,
        items: `Tarjeta de Regalo: ${prettyAmount}`,
        total: amount
      });

      Mailer.send({
        to: email,
        subject:
          "[Abril Bienestar Integral] Haz recibido un certificado de regalo",
        template: "giftCard",
        data: {
          amount: prettyAmount,
          code,
          sender: name || "Anónimo"
        }
      });
    } catch (error) {
      console.log(error);
      if (promoCodeId) {
        PromoCodes.remove(promoCodeId);
      }
      throw new Meteor.Error(
        "invalid-purchase",
        "El método de pago fue rechazado. Verifica tus datos e intenta de nuevo"
      );
    }
  },
  redeemGiftCard: code => {
    const promoCode = PromoCodes.findOne({ code });
    if (!promoCode || promoCode.type !== "giftCard") {
      throw new Meteor.Error("invalid-code", "El certificado es incorrecto");
    }
    Clients.update(
      { userId: Meteor.userId() },
      { $inc: { giftCardAmount: promoCode.amount } }
    );
    PromoCodes.remove(promoCode._id);
  }
});
