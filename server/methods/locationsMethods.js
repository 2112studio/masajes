import { Meteor } from "meteor/meteor";
import PromoCodes from "../../collections/PromoCodes";
import Clients from "../../collections/Clients";
import Locations from "../../collections/Locations";
import Sessions from "../../collections/Sessions";

Meteor.methods({
  removeLocations: selector => {
    const ids = Locations.find(selector).map(item => item._id);
    if (Sessions.find({ locationId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "has-dependency",
        `Item can't be deleted because it has dependency`
      );
    }
    if (Clients.find({ locationId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "has-dependency",
        `Item can't be deleted because it has dependency`
      );
    }
    if (PromoCodes.find({ locationsId: { $in: ids } }).count()) {
      throw new Meteor.Error(
        "has-orders",
        `Item can't be deleted because it has dependency`
      );
    }
  },
  toggleActiveLocation: _id => {
    let active = Locations.findOne(_id).active;
    Locations.update(_id, { $set: { active: !active } });
  }
});
