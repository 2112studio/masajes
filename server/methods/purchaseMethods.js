import { Meteor } from "meteor/meteor";
import conekta from "conekta";
import times from "lodash.times";
import Clients from "../../collections/Clients";
import Sessions from "../../collections/Sessions";
import Orders from "../../collections/Orders";
import products from "../../common/products";
import PromoCodes from "../../collections/PromoCodes";
import { getPurchasePrice } from "../../common/businessLogic";

Meteor.methods({
  purchase: ({
    product: type,
    date: pastDate,
    promoCodeId,
    hasAddress,
    splitPayment,
    useGiftCard,
    userId,
    treatmentGoal,
    creditCardInfo: { sameCard, token, last4 },
    address,
    userData,
    orderSummary
  }) => {
    let date;
    let isNewUser = false;
    if (!type) {
      throw new Meteor.Error("invalid-purchase", "El producto es invalido");
    }
    if (!Meteor.userId()) {
      userId = Meteor.call("createClient", userData, address, hasAddress);
      if (!userId) {
        throw new Meteor.Error(
          "invalid-client",
          "Hubo un error en tu orden. Intenta de nuevo"
        );
      }
      isNewUser = true;
    }
    const product = products.find(({ slug }) => slug === type);
    let client = Clients.findOne({ userId });
    let conektaCustomer;
    if (!client) {
      Meteor.users.remove(userId);
      throw new Meteor.Error(
        "invalid-client",
        "Hubo un error en tu orden. Intenta de nuevo"
      );
    }
    // Prevent any undefined error
    const { address: { locationId = "" } = { address: {} } } = client;

    const promoCode = promoCodeId ? PromoCodes.findOne(promoCodeId) : null;
    if (promoCode)
      Meteor.call("validatePromoCode", {
        code: promoCode.code,
        isNewUser,
        locationId: hasAddress && locationId
      });

    const {
      originalPrice,
      discounted,
      finalPrice,
      giftCardUsed
    } = getPurchasePrice({
      product,
      promoCode,
      splitPayment,
      giftCardAmount: useGiftCard ? client.giftCardAmount : 0
    });

    try {
      const clientUpdateModifier = {
        $set: { isNewUser: false },
        $inc: { giftCardAmount: -giftCardUsed }
      };
      let orderId;
      if (finalPrice > 0) {
        const res = Meteor.call("conektaSetUpPayment", {
          client,
          token,
          last4,
          sameCard
        });
        client = res.client;
        conektaCustomer = res.conektaCustomer;

        orderId = Meteor.call("setConektaOrder", {
          product,
          total: finalPrice,
          client,
          productDescription: product.isTreatment
            ? splitPayment
              ? "Pago 1"
              : "Pago completo"
            : ""
        });
      }
      if (promoCodeId) {
        Meteor.call("redeemPromocode", promoCodeId, type);
      }
      switch (type) {
        case "cuerpomente":
          date = moment(pastDate, "DD/MM/YYYY k:mm").toISOString();

          Sessions.insert({
            type,
            clientId: client._id,
            scheduledDate: date,
            orderId,
            hasAddress,
            locationId,
            status: "confirmed"
          });
          break;
        case "pbi":
          date = pastDate.map(i => moment(i, "DD/MM/YYYY k:mm").toISOString());

          const firstSession = Sessions.insert({
            type,
            clientId: client._id,
            scheduledDate: date[0],
            orderId,
            hasAddress,
            locationId,
            isTreatment: true,
            status: "confirmed"
          });
          const restSessions = times(product.numberOfSessions - 1, index =>
            Sessions.insert({
              type,
              clientId: client._id,
              scheduledDate: date[index + 1],
              orderId,
              hasAddress,
              locationId,
              isTreatment: true,
              status: "confirmed"
            })
          );
          clientUpdateModifier.$push = {
            treatments: {
              name: product.title,
              goal: treatmentGoal,
              type,
              startDate: date[0],
              sessions: [firstSession, ...restSessions],
              isCurrent: true,
              amountPaid: originalPrice,
              discountsApplied: discounted,
              paidInFull: !splitPayment
            }
          };
          break;
        default:
          break;
      }
      Clients.update({ _id: client._id }, clientUpdateModifier);

      if (isNewUser) {
        Mailer.send({
          to: client.email,
          subject: `[Abril Bienestar Integral] Bienvenido!`,
          template: "welcome",
          data: userData
        });
      }
      Mailer.send({
        to: client.email,
        subject: "[Abril Bienestar Integral] Compra exitosa",
        template: "newOrderClient",
        data: { firstname: client.firstname, orderSummary }
      });
      Mailer.send({
        to: process.env.ADMIN_EMAIL,
        subject: "[Abril Bienestar Integral] Nueva compra",
        template: "newOrderAdmin",
        data: { orderSummary }
      });
    } catch (error) {
      console.log(error);
      if (isNewUser & conektaCustomer) {
        Clients.remove({ _id: client._id });
        Meteor.wrapAsync(conektaCustomer.delete, conektaCustomer)();
      }
      throw new Meteor.Error(
        "payment-error",
        error.reason || "Hubo un error en la orden. Intenta de nuevo"
      );
    }
  },
  payTreatment: ({ sameCard, token, last4, noPayment, useGiftCard }) => {
    let client = Clients.findOne({ userId: Meteor.userId() }) || {};
    const { treatments = [], _id: clientId, pendingPromos } = client;
    const treatmentIndex = treatments.findIndex(({ isCurrent }) => isCurrent);
    if (treatmentIndex < 0) {
      throw new Meteor.Error("invalid-purchase", "Hubo un error en la compra");
    }
    const { type, amountPaid, sessions } = treatments[treatmentIndex];
    const sessionIndex = sessions.findIndex(
      i => Sessions.findOne(i).status === "confirmed"
    );
    // Session 1 is already paid
    if (sessionIndex <= 0) {
      throw new Meteor.Error("invalid-purchase", "Hubo un error en la compra");
    }
    const product = products.find(({ slug }) => slug === type);
    const {
      partialPayments,
      partialPaymentsSessions,
      price,
      numberOfSessions
    } = product;
    const paymentNumberIndex = partialPaymentsSessions.findIndex(
      i => i === sessionIndex + 1
    );
    if (paymentNumberIndex < 0) {
      throw new Meteor.Error("invalid-purchase", "Hubo un error en la compra");
    }
    let numberOfSessionsPaid;
    let amountToPay = 0;
    if (paymentNumberIndex === partialPaymentsSessions.length - 1) {
      numberOfSessionsPaid = numberOfSessions;
      const reducedPrice = Meteor.call("getFinalReferralPrice", type, clientId);
      amountToPay = reducedPrice - amountPaid;
      treatments[treatmentIndex].paidInFull = true;
    } else {
      numberOfSessionsPaid =
        partialPaymentsSessions[paymentNumberIndex + 1] - 1;
      amountToPay = price * partialPayments[paymentNumberIndex];
    }

    // Verify that user has enough money to not pay
    if (noPayment) {
      const { giftCardAmount } = client;
      if (giftCardAmount >= amountToPay) {
        Clients.update(clientId, { $inc: { giftCardAmount: -amountToPay } });
      } else {
        throw new Meteor.Error(
          "invalid-purchase",
          "Hubo un error en la compra"
        );
      }
    } else {
      let creditCardAmount = amountToPay;
      // Subtract gift card money to total and update users money
      if (useGiftCard) {
        const { giftCardAmount } = client;
        creditCardAmount -= giftCardAmount;
        Clients.update(clientId, {
          $inc: {
            giftCardAmount:
              giftCardAmount > amountToPay ? -amountToPay : -giftCardAmount
          }
        });
        treatments[treatmentIndex].discountsApplied +=
          giftCardAmount > amountToPay ? amountToPay : giftCardAmount;
      }

      if (creditCardAmount > 0) {
        const res = Meteor.call("conektaSetUpPayment", {
          client,
          token,
          last4,
          sameCard
        });
        client = res.client;
        Meteor.call("setConektaOrder", {
          product,
          total: creditCardAmount,
          client,
          productDescription: `Pago ${paymentNumberIndex + 1}`
        });
      }
    }

    times(numberOfSessionsPaid, i => {
      Sessions.update(sessions[i], {
        $set: { status: "confirmed" }
      });
    });
    treatments[treatmentIndex].amountPaid += amountToPay;
    times(
      pendingPromos.length > numberOfSessions
        ? numberOfSessions
        : pendingPromos.length,
      () => {
        pendingPromos.pop();
      }
    );
    Clients.update(clientId, { $set: { treatments, pendingPromos } });
    Mailer.send({
      to: client.email,
      subject: "[Abril Bienestar Integral] Pago exitoso",
      template: "nextSession",
      data: { firstname: client.firstname, price: amountToPay }
    });
  },
  conektaSetUpPayment: ({ client, token, last4, sameCard }) => {
    let conektaCustomer;
    if (!client.conektaId) {
      const res = Meteor.call("addConektaClient", token, client.userId);
      const { id: conektaId } = res;
      conektaCustomer = res.conektaCustomer;
      client.conektaId = conektaId;
    } else if (client.last4 && token) {
      Meteor.call("changeConektaCard", token, client, last4);
    }
    if (!sameCard) {
      Clients.update(client._id, { $set: { last4 } });
    }
    return { client, conektaCustomer };
  },
  addConektaClient: (token, userId) => {
    const { _id, firstname, lastname, email, phone } = Clients.findOne({
      userId
    });

    try {
      const customer = Meteor.wrapAsync(
        conekta.Customer.create,
        conekta.Customer
      )({
        name: `${firstname} ${lastname}`,
        email,
        phone,
        payment_sources: [
          {
            type: "card",
            token_id: token
          }
        ]
      });
      const { id } = customer.toObject();
      Clients.update(_id, { $set: { conektaId: id } });
      return { id, conektaCustomer: customer };
    } catch (error) {
      console.log(error);
      throw new Meteor.Error(
        "invalid-purchase",
        "Los datos del cliente son inválidos. Intenta de nuevo"
      );
    }
  },
  setConektaOrder: ({
    product: { slug, title },
    total,
    client,
    productDescription = ""
  }) => {
    try {
      const order = Meteor.wrapAsync(conekta.Order.create, conekta.Order)({
        line_items: [
          {
            name: `${title} ${productDescription}`,
            unit_price: total * 100,
            quantity: 1
          }
        ],
        currency: "MXN",
        customer_info: {
          customer_id: client.conektaId
        },
        charges: [
          {
            payment_method: {
              type: "default"
            }
          }
        ]
      });
      const { id } = order.toObject();
      return Orders.insert({
        transactionId: id,
        items: slug,
        total,
        clientId: client._id
      });
    } catch (error) {
      console.log(error);
      throw new Meteor.Error(
        "invalid-purchase",
        "El método de pago fue rechazado. Intenta de nuevo"
      );
    }
  },
  changeConektaCard: (newToken, { _id, conektaId }, last4) => {
    try {
      const customer = Meteor.wrapAsync(
        conekta.Customer.find,
        conekta.Customer
      )(conektaId);
      Meteor.wrapAsync(
        customer.payment_sources.get(0).delete,
        customer.payment_sources
      )();
      Meteor.wrapAsync(customer.createPaymentSource, customer)({
        type: "card",
        token_id: newToken
      });
      Clients.update(_id, { $set: { last4 } });
    } catch (error) {
      console.log(error);
      throw new Meteor.Error(
        "invalid-purchase",
        "Ha habido un error en el método de pago. Intenta de nuevo"
      );
    }
  }
});
