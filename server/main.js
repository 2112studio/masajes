import { Meteor } from "meteor/meteor";
import conekta from "conekta";
import "./accounts";
import "./methods";
import "./publish";
import { pendingStatus } from "../common/businessLogic";
import Templates from "./lib/templates";
import TemplateHelpers from "./lib/templates-helpers";

Meteor.startup(() => {
  SyncedCron.start();
  conekta.api_key = process.env.CONEKTA_PRIVATE;
  conekta.api_version = "2.0.0";
  conekta.locale = "es";

  Clients.find({})
    .fetch()
    .map(client => {
      const modifier = { $set: {} };
      if (!client.treatments) modifier.$set.treatments = [];
      if (!client.updatedAt) modifier.$set.updatedAt = client.createdAt;
      if (!client.giftCardAmount) modifier.$set.giftCardAmount = 0;
      if (Object.keys(modifier.$set).length) {
        Clients.update(client._id, modifier);
      }
    });
  PromoCodes.find({})
    .fetch()
    .map(promo => {
      const modifier = { $set: {} };
      if (!promo.locationsId) modifier.$set.locationsId = [];
      if (promo.isOneTime === undefined) modifier.$set.isOneTime = false;
      if (promo.validCenter === undefined) modifier.$set.validCenter = true;
      if (Object.keys(modifier.$set).length) {
        PromoCodes.update(promo._id, modifier);
      }
    });
});

SyncedCron.add({
  name: "Send reminders",
  schedule: parser =>
    parser
      .recur()
      .every(15)
      .minute(),
  // .second(),
  job: () => {
    const timeNow = moment();
    Sessions.find({ status: { $in: pendingStatus } }).map(
      ({
        _id,
        status,
        scheduledDate,
        reminderDay,
        reminderTwoDay,
        clientId
      }) => {
        const sessionTime = moment(scheduledDate);
        let updateStatus = null;
        let sessionModifier = { $set: {} };
        // Check if session delayed too much and needs to be canceled
        if (status === "ongoing" && sessionTime.add(4, "h").isBefore(timeNow)) {
          updateStatus = "canceled";
          // Check if session needs to start now
        } else if (sessionTime.isBefore(timeNow)) {
          updateStatus = "ongoing";
          // Check if session is within timespan to start
        } else if (
          status === "confirmed" &&
          sessionTime.subtract(30, "minutes").isBefore(timeNow)
        ) {
          updateStatus = "ready";
        }
        // Check if status needs to be updated and if so, update it
        if (updateStatus) {
          sessionModifier.$set.status = updateStatus;
        }

        if (sessionTime.subtract(2, "d").isBefore(timeNow) && !reminderTwoDay) {
          sessionModifier.$set.reminderTwoDay = true;
          const client = Clients.findOne(clientId);
          Mailer.send({
            to: client.email,
            subject:
              "[Abril Bienestar Integral] Recordatorio: Tu Masaje Terapéutico CuerpoMente! Será  en 2 días!",
            template: "reminderTwoDay",
            data: { firstname: client.firstname }
          });
        } else if (
          sessionTime.subtract(1, "d").isBefore(timeNow) &&
          !reminderDay
        ) {
          sessionModifier.$set.reminderDay = true;
          const client = Clients.findOne(clientId);
          Mailer.send({
            to: client.email,
            subject:
              "[Abril Bienestar Integral] Recordatorio: Mañana es día de Masaje Terapéutico CuerpoMente!",
            template: "reminderDay",
            data: { firstname: client.firstname }
          });
        }
        Sessions.update({ _id }, sessionModifier);
      }
    );
  }
});

Mailer.config({
  from: `[Abril Bienestar Integral] <${process.env.SENDER_EMAIL}>`,
  replyTo: `[Abril Bienestar Integral] <${process.env.SENDER_EMAIL}>`,
  plainTextOpts: {
    ignoreImage: true
  }
});

Mailer.init({
  templates: Templates, // Global Templates namespace, see lib/templates.js.
  helpers: TemplateHelpers, // Global template helper namespace.
  layout: {
    name: "emailLayout",
    path: "emailTemplates/layout.html", // Relative to 'private' dir.
    scss: "emailTemplates/sass/layout.scss"
  }
});
