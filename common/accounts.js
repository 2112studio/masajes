import { Template } from "meteor/templating";

// https://github.com/meteor-useraccounts/flow-routing
AccountsTemplates.configure({
  showForgotPasswordLink: true,
  defaultLayout: "LandingLayout",
  defaultTemplate: "Login",
  defaultContentRegion: "main",
  defaultLayoutRegions: {},
  homeRoutePath: "/admin"
});

AccountsTemplates.configureRoute("signIn", {
  name: "signin",
  path: "/login"
});

AccountsTemplates.configureRoute("signUp", {
  name: "join",
  path: "/registrate"
});

AccountsTemplates.configureRoute("forgotPwd", {
  name: "forgotPwd",
  path: "/recuperarCuenta"
});

AccountsTemplates.configureRoute("resetPwd", {
  name: "resetPwd",
  path: "/recuperarContraseña"
});

AccountsTemplates.configureRoute("enrollAccount", {
  name: "enrollAccount",
  path: "/configurarCuenta"
});

// Remove fields first, so we can show name inputs first
const emailField = AccountsTemplates.removeField("email");
const passwordField = AccountsTemplates.removeField("password");

AccountsTemplates.addField({
  _id: "firstname",
  type: "text",
  displayName: "firstname",
  placeholder: "firstname",
  required: true,
  minLength: 3
});
AccountsTemplates.addField({
  _id: "lastname",
  type: "text",
  displayName: "lastname",
  placeholder: "lastname",
  required: true,
  minLength: 3
});
AccountsTemplates.addField(emailField);
AccountsTemplates.addField(passwordField);
