const products = [
  {
    slug: "cuerpomente", // NEVER CHANGE THIS. Is the main key and it's never displayed anyway
    isTreatment: false,
    title: "CuerpoMente®",
    price: 1080,
    firstTimePrice: 576,
    referralDiscounts: [50],
    landingInfo: {
      image: "https://habit.com/images/pages/how-it-works-bottle.jpg",
      pageRoute: "cuerpomente",
      description:
        "Es un masaje Terapéutico  que busca llevarte a través de una exploración de los mensajes que tu cuerpo tiene para ti en una experiencia de profunda relajación Integral y un divertido Aprendizaje de ti mismo. <br>(Duración 60 min de masaje y 20 de Aprendizaje de Hábitos en Bienestar)",
      heroTitle: "CuerpoMente®",
      heroBody: `
Es un masaje terapéutico que entiende que tú, como ser humano, habitas en un cuerpo, experimentas emociones y tienes maneras de pensar, llevándote a través de una exploración de los mensajes que tu cuerpo tiene para ti  en una experiencia de profunda relajación Integral y  un divertido Aprendizaje de ti mismo.
<br><br>¡Porque eres más que solo un cuerpo!
<br><br>(Duración 60 min de masaje y 20 de Aprendizaje de Hábitos en Bienestar)

     `,
      testimonials: [
        {
          index: 0,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Alma Lucia Carmona",
          text:
            "No pensé que un masaje tendría tantos beneficios en mi vida… Se ha vuelto un hábito en mi vida darme este espacio para mí"
        },
        {
          index: 1,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Chivis Trava",
          text:
            "CuerpoMente me ayudó no sólo a relajar mi cuerpo, sino también  a hacerme consciente de mis emociones, también a relajarme y estar en armonía"
        },
        {
          index: 2,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Jesús Garza",
          text:
            "Me ayudó a ver como reaccionaba ante el Estrés al cual estoy expuesto todos los días, me ha ayudado a identificarlo y a manejarlo mejor en mi vida diaria"
        },
        {
          index: 3,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: " Emy Danielle",
          text:
            "Siento que este masaje, es  terapéutico, ya que me ayuda a tomar consciencia de lo que mi cuerpo me quiere decir, siento que verdaderamente estoy aprendiendo a escucharlo y a la vez me siento flotando cada vez que salgo de aquí"
        },
        {
          index: 4,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Hugo Vargas",
          text:
            "El masaje CuerpoMente me ayudó a controlar mis impulsos, enojos y conocer los mensajes que da mi cuerpo"
        },
        {
          index: 5,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Miguel Bernal",
          text:
            "Cuerpo Mente es un masaje bastante completo que aparte de ser relajante ayuda a mejorar el desempeño físico, resistencia, fuerza y mi postura a la hora de entrenar."
        }
      ]
    }
  },
  {
    slug: "pbi", // NEVER CHANGE THIS. Is the main key and it's never displayed anyway
    isTreatment: true,
    title: "Circuito CuerpoMente® (4 masajes)",
    price: 4320,
    firstTimePrice: 3744,
    referencePrice: 3169,
    numberOfSessions: 4,
    sessionNames: [
      "Circuito Integral de Valoración",
      "Circuito Cuerpo",
      "Circuito Mente",
      "Circuito Emociones",
      "Circuito de Mantenimiento"
    ],
    referralDiscounts: [80, 180, 300, 575],
    partialPayments: [0.5, 0.5],
    discountsText: `
      Comparte tu código y acumula hasta $575 de descuento en cada Circuito. <br><br>
      ¡Entre más lo compartas mayores descuentos! <br><br>
      Cantidad de invitados: <br><br>
      1 - $80<br>
      2 - $180<br>
      3 - $300<br>
      4 - $575<br><br>
      *Tu descuento es válido una vez que tus invitados asisten a su primer masaje.
    `,
    partialPaymentsSessions: [1, 3],
    landingInfo: {
      image: "https://habit.com/images/pages/how-it-works-bottle.jpg",
      description:
        "Aprende la relación entre tu Cuerpo, Emociones y Maneras de pensar.  Basado en el masaje terapéutico CuerpoMente® y en la incorporación de hábitos de bienestar  nuestros Programas de Bienestar Integral te llevarán de la mano para la construcción de la mejor versión de ti.",
      pageRoute: "pbi",
      heroTitle: "Programa de  Bienestar Integral ®",
      heroBody: `
      Aprenderás  la relación entre tu Cuerpo, Emociones y Maneras de pensar acompañándote en una  relajación Integral.
<br><br>Basado en el masaje terapéutico CuerpoMente® y en la incorporación de hábitos de bienestar que te llevarán de la mano para la construcción de la mejor versión de ti.
<br><br>La Experiencia será no sólo de una profunda relajación Integral, sino también de un divertido Aprendizaje de ti mismo.
     `,
      steps: [
        {
          title: "Conócenos",
          icon: "/img/excelente.svg",
          text:
            "Experimenta nuestro masaje terapéutico CuerpoMente® (Duración 60 min de masaje y 20 de Aprendizaje de Hábitos en Bienestar)"
        },
        {
          title: "Déjanos Conocerte",
          icon: "/img/excelente.svg",
          text:
            "Establece tu objetivos y metas para la creación de la mejor versión de ti"
        },
        {
          title: "Circuito Integral de Valoración (0)",
          icon: "/img/excelente.svg",
          text: `
         Juntos identificaremos las áreas de mayor congestión en tu cuerpo en un circuito de 4 masajes (1 por semana)
         en donde irás incorporando hábitos y haremos una valoración integral para diseñar tus siguientes circuitos de masaje <br>
         Duración: 1 Mes
         `
        },
        {
          title: "Profundicemos! (1,2,3,)",
          icon: "/img/excelente.svg",
          text: `
         Juntos diseñaremos tus circuitos de Masajes en donde <br>
         1- Haremos una Relajación Integral <br>
         2- Exploraremos los hábitos en tu Cuerpo, Mente y Emoción y aprenderemos como se relacionan.  <br>
         3- Te acompañaremos en una incorporación de Hábitos de Bienestar para la construcción de una mejor versión de ti. <br>
         Duración: 3 Meses
         `
        }
      ],
      testimonials: [
        {
          index: 0,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Alma Lucia Carmona",
          text:
            "No pensé que un masaje tendría tantos beneficios en mi vida… Se ha vuelto un hábito en mi vida darme este espacio para mí"
        },
        {
          index: 1,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Chivis Trava",
          text:
            "CuerpoMente me ayudó no sólo a relajar mi cuerpo, sino también  a hacerme consciente de mis emociones, también a relajarme y estar en armonía"
        },
        {
          index: 2,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Jesús Garza",
          text:
            "Me ayudó a ver como reaccionaba ante el Estrés al cual estoy expuesto todos los días, me ha ayudado a identificarlo y a manejarlo mejor en mi vida diaria"
        },
        {
          index: 3,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: " Emy Danielle",
          text:
            "Siento que este masaje, es  terapéutico, ya que me ayuda a tomar consciencia de lo que mi cuerpo me quiere decir, siento que verdaderamente estoy aprendiendo a escucharlo y a la vez me siento flotando cada vez que salgo de aquí"
        },
        {
          index: 4,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Hugo Vargas",
          text:
            "El masaje CuerpoMente me ayudó a controlar mis impulsos, enojos y conocer los mensajes que da mi cuerpo"
        },
        {
          index: 5,
          picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
          name: "Miguel Bernal",
          text:
            "Cuerpo Mente es un masaje bastante completo que aparte de ser relajante ayuda a mejorar el desempeño físico, resistencia, fuerza y mi postura a la hora de entrenar."
        }
      ],
      faq: [
        {
          question:
            "¿En qué se diferencía un masaje normal a un masaje terapéutico?",
          answer:
            "Un masaje terapéutico tiene una visión integral del SER, esto quiere decir que entiende que el ser humano tiene un cuerpo, siente emociones, y tiene maneras de pensar o creencias, vive en un entorno, entre otros factores, al tener esta visión entiende que hay muchos puntos de entrada que requieren trabajo de relajación, es por eso que le llamamos relajación integral y no sólo el cuerpo en el cual se enfoca el masaje de SPA regularmente."
        },
        {
          question:
            "¿Cómo funciona la incorporación de Hábitos de Bienestar en el programa?",
          answer: `
Nos dimos cuenta de que estar relajado no se trata de estar tomando masajes, sino más bien es una serie de construir y sostener hábitos saludables que dan como resultado una vida de Bienestar, por lo tanto decidimos enfocarnos en ayudar a que  las personas a lograr esto.

El proceso de Aprendizaje se da en el momento de recibir el masaje y por tu cuenta, al hacer las prácticas  de incorporación de hábitos

`
        },
        {
          question:
            "¿Que voy a Aprender  en los Programas de Bienestar Integral ®?",
          answer: `
          Terminarás nuestros programas con un botiquin de bienestar lleno de prácticas como automasajes, meditaciones, visualizaciones, reflexiones, posturas, ejercicios físicos que serán parte de tu vida diaria.

          `
        },
        {
          question:
            "¿Cuánto tiempo voy a dedicarle al Programa de Bienestar Integral ®?",
          answer:
            "1 masaje terapéutico a la semana y 10-25 min diarios de incorporación de hábitos dependiendo del tiempo que desees invertir."
        },
        {
          question: "¿Es indispensable que asista 1 vez a la semana a masaje?",
          answer:
            "No es una imposición, más bien una fuerte recomendación, nuestros Programas de Bienestar están pensado así al menos en los primeros circuitos de masaje para lograr una profunda relajación integral y un seguimiento.  Conforme vayas avanzando podrás ir espaciando tus masajes, hasta quedarte en una fase de mantenimiento."
        },
        {
          question: "¿El masaje Terapéutico CuerpoMente Duele?",
          answer:
            "Es un masaje de tejido profundo, es por eso que pueden haber puntos en tu cuerpo que duelan. Ese dolor no es más que una tensión con la cual vas cargando en tu vida diaria al cual muy probablemente te has acostumbrado  y no tanto un dolor que generemos con  el masaje, el dolor más que ser algo que lastime es un proceso de desbloqueo para generar un efecto prolongado de relajación muscular."
        },
        {
          question: "¿Abril Bienestar Integral es una escuela o un SPA?",
          answer:
            "No somos un SPA como tal, tampoco somos fisioterapeutas o doctores, somos un instituto en donde ayudamos a las personas a incorporar hábitos de bienestar en sus vidas, es por esa razón que entendemos que debe de haber un aprendizaje y un acompañamiento en el mismo proceso."
        },
        {
          question: "¿Profesamos alguna religión?",
          answer:
            "No profesamos ninguna religión, consideramos que todos somos seres humanos tenemos un mismo cuerpo, ganas de mejorar y vivir vidas en Bienestar."
        }
      ]
    }
  }
];

export default products;
