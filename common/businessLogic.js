export const pendingStatus = ["confirmed", "ready", "ongoing"];
export const pastStatus = ["canceled", "completed"];
export const statusDict = [
  { value: "unconfirmed", label: "No pagada" },
  { value: "confirmed", label: "Confirmada" },
  { value: "ready", label: "Lista para iniciar" },
  { value: "ongoing", label: "En progreso" },
  { value: "canceled", label: "Cancelada" },
  { value: "completed", label: "Completada" }
];
export const surveyQuestions = [
  {
    slug: "pain",
    type: "string",
    label: "¿Hay algún dolor en tu cuerpo?",
    resultsLabel: "Te duele:",
    required: true,
    bank: {
      pies: "Pies",
      chamorros: "Chamorros",
      rodillas: "Rodillas",
      muslos: "Muslos",
      sacro: "Sacro",
      pelvis: "Pelvis",
      espalda: "Espalda",
      brazos: "Brazos",
      manos: "Manos",
      vientre: "Vientre",
      pecho: "Pecho",
      cuello: "Cuello",
      cabeza: "Cabeza",
      cara: "Cara"
    }
  },
  {
    slug: "feelings",
    type: "options",
    label: "¿Cómo te sientes?",
    resultsLabel: "Te sientes:",
    required: true,
    options: [
      {
        label: "Content@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Ansios@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Preocupad@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Relajad@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Autentic@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Triste",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Miedos@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Confiad@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Enojad@",
        icon: "/img/excelente.svg",
        slug: "feelings"
      },
      {
        label: "Paciente",
        icon: "/img/excelente.svg",
        slug: "feelings"
      }
    ]
  },
  {
    slug: "thoughts",
    resultsLabel: "Piensas:",
    type: "string",
    label: "¿En qué estás pensando?",
    required: true,
    bank: {
      familia: "Familia",
      salud: "Salud",
      dinero: "Dinero",
      trabajo: "Trabajo",
      estudio: "Estudio",
      pareja: "Pareja",
      metas: "Metas",
      negocio: "Negocio",
      amigos: "Amigos",
      recreacion: "Recreación"
    }
  }
];
export const generalTestimonies = [
  {
    index: 0,
    picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
    name: "Michelle H",
    text:
      "I've lost the weight I struggled with, I feel fitter and I'm getting the right fuel for my body every day."
  },
  {
    index: 1,
    picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
    name: "Mike M",
    text:
      "I've noticed that my clothes are looser on my body, I feel better. I noticed that I have more energy."
  },
  {
    index: 2,
    picture: "https://habit.com/images/testimonials/testimonials-kim.jpg",
    name: "Kimberly D",
    text:
      "I noticed immediate improvements in energy levels and even found myself sleeping better."
  }
];
export const getPurchasePrice = ({
  product,
  promoCode,
  splitPayment,
  giftCardAmount = 0
}) => {
  if (product) {
    let total = product.price;
    const res = { originalPrice: product.price, discounted: 0 };
    if (product.isTreatment && splitPayment) {
      total = product.partialPayments[0] * product.price;
      res.originalPrice = total;
      if (promoCode && promoCode.type !== "newClients") {
        switch (promoCode.type) {
          case "amount":
            res.discounted = promoCode.amount;
            total -= res.discounted;
            break;
          case "percentage":
            res.discounted = total * promoCode.amount * 0.01;
            total *= 1 - promoCode.amount * 0.01;
            break;
        }
      }
    } else if (promoCode) {
      switch (promoCode.type) {
        case "newClients":
          total = product.firstTimePrice;
          break;
        case "amount":
          total -= promoCode.amount;
          break;
        case "percentage":
          total *= 1 - promoCode.amount * 0.01;
          break;
      }
    }
    if (giftCardAmount > total) {
      res.giftCardUsed = total;
    } else {
      res.giftCardUsed = giftCardAmount;
    }
    res.discounted += res.giftCardUsed;
    res.finalPrice = total - giftCardAmount < 0 ? 0 : total - giftCardAmount;
    return res;
  }
  return false;
};

export const accountFields = [
  { id: "firstname", label: "Nombre(s)" },
  { id: "lastname", label: "Apellidos" },
  { id: "email", label: "Email", type: "email" },
  { id: "password", label: "Contraseña", type: "password" },
  { id: "repeatPassword", label: "Repetir contraseña", type: "password" }
];
export const addressFields = [
  { id: "street1", label: "Calle y Número" },
  { id: "street2", label: "Colonia" },
  { id: "zip", label: "Código postal" },
  { id: "reference", label: "Referencias del lugar o entre qué calles" },
  { id: "locationId", hidden: true }
];
